Django==3.2.4
beautifulsoup4==4.11.2
celery==5.1.2
redis==3.5.3
django-cors-headers==3.12.0
django-environ==0.8.1
django-filter==22.1
django-jazzmin==2.5.0
django-modeltranslation==0.18.4
django-mptt==0.13.4
django-rest-swagger==2.1.1
django-solo==2.0.0
djangorestframework==3.13.1
djangorestframework-recursive==0.1.2
djangorestframework-simplejwt==5.2.2
django-reverse-admin==2.9.6
drf-yasg==1.20.0
Pillow==9.1.1
psycopg2-binary==2.9.3
requests==2.28.1
django-celery-beat
flower==1.2.0
