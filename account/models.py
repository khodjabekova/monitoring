from django.contrib.auth.models import AbstractUser
from django.db import models



class CustomUser(AbstractUser):

    username = models.CharField(max_length=255, unique=True)
    is_staff = models.BooleanField(default=False)
    email = models.CharField(max_length=255, null=True, blank=True)



    def save(self, *args, **kwargs):
        try:
            if kwargs['password']:
                self.set_password(kwargs['password'])
        except Exception:
            pass
        finally:
            super(CustomUser, self).save(*args, **kwargs)


    class Meta:
        db_table = 'account'
        verbose_name = "Foydalanuvchi"
        verbose_name_plural = "Foydalanuvchilar"


