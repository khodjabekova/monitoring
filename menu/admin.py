from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from .models import Menu
from django import forms


class MenutForm(forms.ModelForm):
    

    class Meta:
        model = Menu
        exclude = (
            'created_by',
            'updated_by',
        )
        widgets = {
            'parent': forms.Select(attrs={'class': 'bootstrap-select', 'data-width': "90%"})
        }


class MenuAdmin(MPTTModelAdmin):
    list_display = ('name', 'index')
    form = MenutForm


admin.site.register(Menu, MenuAdmin)