from django.db import models
from baseapp.models import BaseModel
from mptt.models import MPTTModel, TreeForeignKey


class Menu(BaseModel, MPTTModel):
    parent = TreeForeignKey('self', on_delete=models.SET_NULL,
                            null=True, blank=True, verbose_name="Yuqori menyu")
    name = models.CharField(max_length=500, verbose_name="Nomi")
    index = models.IntegerField(
            null=True, blank=True, verbose_name="Tartib raqami")
    def __str__(self):
        return self.name

    class Meta:
        ordering = ['index']
        db_table = 'menu'
        verbose_name = "Menyu"
        verbose_name_plural = "Menyu"