# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-30 16:57+0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .\templates\admin\about\submit_line.html:15
#: .\templates\admin\submit_line.html:15
msgid "Save"
msgstr ""

#: .\templates\admin\about\submit_line.html:21
#: .\templates\admin\submit_line.html:21
msgid "Delete"
msgstr ""

#: .\templates\admin\about\submit_line.html:26
#: .\templates\admin\submit_line.html:26
msgid "Save as new"
msgstr ""

#: .\templates\admin\about\submit_line.html:36
#: .\templates\admin\submit_line.html:36
msgid "Save and view"
msgstr ""

#: .\templates\admin\base.html:151
msgid "Search"
msgstr ""

#: .\templates\admin\base.html:211
msgid "Account"
msgstr ""

#: .\templates\admin\base.html:214
msgid "Change password"
msgstr ""

#: .\templates\admin\base.html:218
msgid "Log out"
msgstr ""

#: .\templates\admin\base.html:229
msgid "See Profile"
msgstr ""

#: .\templates\admin\base.html:389
msgid "Jazzmin version"
msgstr ""

#: .\templates\admin\base.html:392
msgid "Copyright"
msgstr ""

#: .\templates\admin\base.html:392
msgid "All rights reserved."
msgstr ""

#: .\templates\admin\base.html:408
msgid "UI Configuration"
msgstr ""

#: .\templates\admin\base.html:412
msgid "Copy this info your settings file to persist these UI changes"
msgstr ""

#: .\templates\admin\base.html:416
msgid "Close"
msgstr ""

#: .\templates\admin\change_form.html:23
#: .\templates\admin\delete_confirmation.html:15
#: .\templates\admin\delete_selected_confirmation.html:15
#: .\templates\admin\index.html:11 .\templates\admin\solo\change_form.html:7
#: .\templates\admin\solo\object_history.html:6
msgid "Home"
msgstr ""

#: .\templates\admin\change_form.html:34
#, python-format
msgid "Add %(name)s"
msgstr ""

#: .\templates\admin\change_form.html:55
msgid "Please correct the error below."
msgstr ""

#: .\templates\admin\change_form.html:57
msgid "Please correct the errors below."
msgstr ""

#: .\templates\admin\delete_confirmation.html:38
#, python-format
msgid ""
"%(object_name)s '%(escaped_object)s'ni o'chirish tegishli ob'ektlarni "
"o'chirishga olib keladi, lekin sizda quyidagi turdagi ob'ektlarni "
"o'chirishga ruxsat yo'q:"
msgstr ""

#: .\templates\admin\delete_confirmation.html:45
#, python-format
msgid ""
"%(object_name)s '%(escaped_object)s'ni o'chirish quyidagi ob'ektlarni "
"o'chirishni talab qiladi:"
msgstr ""

#: .\templates\admin\delete_confirmation.html:52
#, python-format
msgid ""
"Haqiqatan ham %(object_name)s \"%(escaped_object)s\" faylini o‘chirib "
"tashlamoqchimisiz? Quyidagi barcha elementlar ham oʻchirib tashlanadi:"
msgstr ""

#: .\templates\admin\delete_selected_confirmation.html:37
#, python-format
msgid ""
"Tanlangan %(objects_name)s ni oʻchirish quyidagi obʼyektlarni oʻchirishga "
"olib keladi, lekin sizda quyidagi turdagi obyektlarni oʻchirishga ruxsat "
"yoʻq:"
msgstr ""

#: .\templates\admin\delete_selected_confirmation.html:44
#, python-format
msgid ""
"Tanlangan %(objects_name)s ni oʻchirish quyidagi  obyektlarni oʻchirishni "
"talab qiladi:"
msgstr ""

#: .\templates\admin\delete_selected_confirmation.html:51
#, python-format
msgid ""
"Haqiqatan ham tanlangan %(objects_name)s ni oʻchirib tashlamoqchimisiz? "
"Quyidagi barcha ob'ektlar va ularga tegishli elementlar o'chiriladi:"
msgstr ""

#: .\templates\admin\delete_selected_confirmation.html:54
#: .\templates\admin\filer\delete_selected_files_confirmation.html:41
msgid "Objects"
msgstr ""

#: .\templates\admin\includes\object_delete_summary.html:2
msgid "Summary"
msgstr ""

#: .\templates\admin\index.html:42
msgid "Add"
msgstr ""

#: .\templates\admin\index.html:46
msgid "View"
msgstr ""

#: .\templates\admin\index.html:48
msgid "Go"
msgstr ""

#: .\templates\admin\index.html:48
msgid "Change"
msgstr ""

#: .\templates\admin\index.html:73
msgid "Recent actions"
msgstr ""

#: .\templates\admin\index.html:77
msgid "None available"
msgstr ""

#: .\templates\admin\index.html:91
#, python-format
msgid "%(timesince)s ago"
msgstr ""

#: .\templates\admin\login.html:14
msgid "Log in again"
msgstr ""
