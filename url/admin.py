from django.contrib import admin
from .models import URL, URLStat
from django.utils import timezone
from django.db.models import OuterRef, Subquery, Q

class LastStatusFilter(admin.SimpleListFilter):
    title = 'Last Status'
    parameter_name = 'last_status'

    def lookups(self, request, model_admin):
        return (
            ('active', 'Active'),
            ('failed', 'Failed'),
            ('unknown', 'Unknown'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'active':
            return queryset.annotate(latest_status=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__startswith='2') | Q(latest_status__startswith='3'))
        elif value == 'failed':
            return queryset.annotate(latest_status=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__startswith='4') | Q(latest_status__startswith='5'))
        elif value == 'unknown':
            return queryset.annotate(latest_status=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__isnull=True))

        return queryset


class URLAdmin(admin.ModelAdmin):
    list_display = ('url', 'website', 'information_system', 'total_down', 'last_status', 'last_status_code', 'www_id')
    exclude = ('created_by', 'updated_by')
    # readonly_fields = ['website', 'information_system', 'last_status', 'last_status_code']
    search_fields = ['url']
    list_filter = ['url', 'website', 'information_system', LastStatusFilter]

    forms = URL


admin.site.register(URL, URLAdmin)


class StatAdmin(admin.ModelAdmin):
    list_display = ('url', 'status_code', 'response_time', 'date', 'down') 
    exclude = ('created_by', 'updated_by')
    # readonly_fields = ['url', 'status_code', 'response_time', 'log', 'date']
    search_fields = ['url', 'status_code', 'created_at']
    list_filter = ['url', 'status_code', 'created_at' ]
    forms = URLStat

    def date(self, obj):
        date = timezone.localtime(obj.created_at)
        return date.strftime("%d-%m-%Y %H:%M:%S")
    date.admin_order_field = 'created_at'
    date.short_description = 'Vaqti'    



admin.site.register(URLStat, StatAdmin)
