import requests
import environ

from subscriber.models import Subscriber
env = environ.Env()
environ.Env.read_env()
from url.statuscode import status_ru
from celery import shared_task
from celery.utils.log import get_task_logger


logger = get_task_logger(__name__)

@shared_task(serializer='json', name="notification")
def notify_on_telegram(url, status_code=None, log=None):
    if status_code:
        # message_html =  f"Notification about failure. URL {url} failed with status code {status_code}"
        if status_code in status_ru:
            message_html =  f"Сообщение об ошибке. URL {url} не прошел с кодом состояния {status_code} ({status_ru[status_code]})"
        else:
            message_html =  f"Сообщение об ошибке. URL {url} не прошел с кодом состояния {status_code}"

    else:
        message_html =  f"Сообщение об ошибке. URL {url} не прошел. Сервер не отвечает."
        # message_html =  f"Notification about failure. URL {url} failed. Log: {log}"
    # telegram_settings = settings.TELEGRAM
    # bot = Bot(token=telegram_settings['bot_token'])
    # print(message_html)
    subscribers = Subscriber.objects.all()
    for s in subscribers:
        # bot.send_message(chat_id="chat_id", text=message_html)
        url = f"https://api.telegram.org/bot{env('TOKEN')}/sendMessage?chat_id={s.chat_id}&text={message_html}"
        requests.get(url).json()

    return 5
