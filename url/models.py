from django.db import models
from baseapp.models import BaseModel
from config.utils import StatusType
# from information_system.models import InformationSystem
# from web_site.models import WebSite
from django.utils.translation import gettext_lazy as _


class URL(BaseModel):
    ONCE_IN_20 = 0
    ONCE_IN_AN_HOUR = 1
    ONCE_IN_A_DAY = 2
    IGNORE = 3

    CHECK_CHOICE = (
        (ONCE_IN_20, _('Har 20 daqiqa')),
        (ONCE_IN_AN_HOUR, _('Har soat')),
        (ONCE_IN_A_DAY,  _('Har 24 soat')),
        (IGNORE,  _('Tekshirilmasin')),
    )
    # website = models.OneToOneField(WebSite, on_delete=models.CASCADE, related_name='url', null=True, blank=True, verbose_name="Veb-sayt")
    # information_system = models.OneToOneField(InformationSystem, on_delete=models.CASCADE, related_name='url', null=True, blank=True, verbose_name="Axborot tizimi")
    url = models.URLField(max_length=255, verbose_name="URL")
    www_id = models.BigIntegerField(null=True, blank=True, verbose_name='www id')
    # last_status = models.CharField(max_length=3,default=200, null=True)
    total_down = models.BigIntegerField(default=0)
    period = models.PositiveSmallIntegerField(choices=CHECK_CHOICE, default=ONCE_IN_20, verbose_name="Tekshirish davri")

    def __str__(self):
        return self.url

    @property
    def last_status(self):
        status_type = StatusType(self.stats.latest('created_at').status_code)
        return status_type

    @property
    def last_status_code(self):
        return self.stats.latest('created_at').status_code
  
    class Meta:
        db_table = 'url'
        verbose_name = "Havola"
        verbose_name_plural = "Havolalar"


class URLStat(models.Model):
    url = models.ForeignKey(URL, on_delete=models.SET_NULL, null=True, blank=True, related_name="stats")
    status_code = models.CharField(max_length=3, null=True, blank=True)
    response_time = models.DecimalField(max_digits=8, decimal_places=6, null=True, blank=True)
    log = models.CharField(max_length=500, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    failed = models.BooleanField(default=False)
    down = models.BigIntegerField(default=0)
    class Meta:
        db_table = 'url_stat'
        verbose_name = "Statistika"
        verbose_name_plural = "Statistika"