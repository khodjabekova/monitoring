import datetime
from config.utils import StatusType
from url.email_notification import send_notification
from url.models import URL, URLStat

from celery import shared_task
from celery.utils.log import get_task_logger
import requests
from requests.adapters import HTTPAdapter, Retry

from django.db.models import OuterRef, Subquery, Q, Count

from url.notification_bot import notify_on_telegram

from requests.adapters import HTTPAdapter, Retry

logger = get_task_logger(__name__)


def check(api_list):

    for api in api_list:  # TODO: timeout for request
        prev = URLStat.objects.filter(url_id=api.id).order_by('created_at').last() 
        prev_status = None
        if prev:
            # prev = prev.latest('created_at')
            prev_status = StatusType(prev.status_code)
        
        down_time = 0
        time_delta = 0
        
                
        try:
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'}
            s = requests.Session()

            retries = Retry(total=5,
                            backoff_factor=0.1,
                            status_forcelist=[ 500, 502, 503, 504 ])

            s.mount('http://', HTTPAdapter(max_retries=retries))

            try:
                response = s.get(api.url, headers=headers)
            except requests.exceptions.SSLError as e:
                response = s.get(api.url, verify=False, headers=headers)
           


            if not response.ok and (not prev_status or prev_status == 'active'):
                
                notify_on_telegram.delay(url=api.url, status_code=response.status_code)

                URLStat.objects.create(url_id=api.id, status_code=response.status_code, failed=True,
                                       response_time=response.elapsed.total_seconds(), created_at=datetime.datetime.now())
            # elif response.ok:
            #     URLStat.objects.create(url_id=api.id, status_code=response.status_code,
            #                            response_time=response.elapsed.total_seconds(), created_at=datetime.datetime.now(), down=down_time)
            
            else:
                if prev_status == 'failed' or prev_status == 'unknown':
                    date_time = datetime.datetime.now()
                    time_delta = (date_time.astimezone() - prev.created_at).total_seconds()//60
                    down_time = prev.down + time_delta
                URLStat.objects.create(url_id=api.id, status_code=response.status_code,
                                       response_time=response.elapsed.total_seconds(), created_at=datetime.datetime.now(), down=down_time)
                

        except Exception as e:
            
            failed = False 
            if not prev_status or prev_status == 'active':
                failed = True
                notify_on_telegram.delay(url=api.url, log=str(e))
           
            if prev_status == 'failed' or prev_status == 'unknown':
                date_time = datetime.datetime.now()
                time_delta = (date_time.astimezone() - prev.created_at).total_seconds()//60
                down_time = prev.down + time_delta
                
            URLStat.objects.create(
                url_id=api.id, log=str(e), created_at=datetime.datetime.now(), failed=failed, down=down_time)
            
        if time_delta > 0:
            api.total_down += time_delta
            api.save()



@shared_task
def api_checker():
    api_list = URL.objects.prefetch_related('stats')\
        .annotate(stats_count = Count('stats'), 
        status=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1]))\
        .filter(period=0).filter(Q(status__startswith='2')|Q(status__startswith='3')|Q(stats_count=0))
 
    check(api_list)
    return 1

@shared_task
def api_checker_hour():
    api_list = URL.objects.prefetch_related('stats')\
        .annotate(stats_count = Count('stats'), 
        status=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1]))\
        .filter(period=1).filter(Q(status__startswith='2')|Q(status__startswith='3')|Q(stats_count=0))
 
    check(api_list)
    return 1

@shared_task
def api_checker_day():
    api_list = URL.objects.prefetch_related('stats')\
        .annotate(stats_count = Count('stats'), 
        status=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1]))\
        .filter(period=2).filter(Q(status__startswith='2')|Q(status__startswith='3')|Q(stats_count=0))
 
    check(api_list)
    return 1

@shared_task
def api_rechecker():
    api_list = URL.objects.prefetch_related('stats')\
        .annotate(latest_down=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('down')[:1]), 
        status=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1]))\
        .filter(Q(status__startswith='4', latest_down__lt=60) | Q(status__startswith='5', latest_down__lt=60) | Q(status__isnull=True, latest_down__lt=60)).exclude(period=3)
    check(api_list)
    return 2


@shared_task
def hourly_rechecker():
    api_list = URL.objects.prefetch_related('stats')\
        .annotate(latest_down=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('down')[:1]), 
        status=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1]))\
        .filter(Q(status__startswith='4',  latest_down__gte=60, latest_down__lt=1440) | Q(status__startswith='5',  latest_down__gte=60, latest_down__lt=1440)  | Q(status__isnull=True, latest_down__gt=60, latest_down__lt=1440)).exclude(period=3)
    check(api_list)
    return 3


@shared_task
def daily_rechecker():
    api_list = URL.objects.prefetch_related('stats')\
        .annotate(latest_down=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('down')[:1]),
        status=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1]))\
        .filter(Q(status__startswith='4', latest_down__gte=1440) | Q(status__startswith='5', latest_down__gte=1440) | Q(status__isnull=True, latest_down__gte=1440)).exclude(period=3)
    check(api_list)
    return 4
