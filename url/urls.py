from django.urls import path

from . import views

urlpatterns = [
    path('url/<pk>', views.UrlStatView.as_view()),
    path('response/<pk>/<param>', views.ResponseStatView.as_view()),
    path('visitors/<pk>', views.VisitorsStatView.as_view()),
    path('status/<pk>', views.StatusStatView.as_view()),
    path('url', views.URLListView.as_view()),
    path('rating', views.URLRatingView.as_view()),
    path('organizations', views.OrganizationsStatView.as_view()),
    path('organizations/total-count', views.OrganizationsTotalCountView.as_view()),
    path('up-down1', views.UpDownStatView.as_view()),
    path('up-down', views.UpDown2StatView.as_view()),
    path('down', views.DownStatView.as_view()),
    path('down2', views.DownStat2View.as_view()),
    path('location', views.LocationStatView.as_view()),
    path('expert', views.ExpertStatView.as_view()),
    path('protocol', views.ProtocolStatView.as_view()),
    path('mobileapp', views.MobileAppStatView.as_view()),
    path('callcentre', views.CallCentreStatView.as_view()),
    path('organization-rating', views.OrganizationRatingStatView.as_view()),
]
