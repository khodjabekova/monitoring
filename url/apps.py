from django.apps import AppConfig


class UrlConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'url'
    verbose_name = "Havola"
    verbose_name_plural = "Havolalar"