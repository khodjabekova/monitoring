from config.utils import StatusType
from organization.models import Organization
from .models import URL, URLStat
from rest_framework import serializers
from datetime import datetime, timedelta


class URLListSerializer(serializers.ModelSerializer):

    class Meta:
        model = URL
        fields = ('id', 'url')


class URLRaitingSerializer(serializers.ModelSerializer):
    failed = serializers.IntegerField()
    url = serializers.CharField(source='url__url')
    class Meta:
        model = URL
        fields = ('id', 'url', 'failed', 'total_down')


class ResponseMonthWeekSerializer(serializers.Serializer):
    created_at = serializers.DateField()
    response_time = serializers.DecimalField(max_digits=30, decimal_places=6)


class ResponseTimeSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format='%H:%M:%S')

    class Meta:
        model = URLStat
        fields = ('response_time', 'created_at')


class VisitorsSerializer(serializers.Serializer):
    hour = serializers.SerializerMethodField()
    visitors = serializers.IntegerField()
    visitors_uz = serializers.IntegerField()

    def get_hour(self, obj):
        return f"{obj['hour']}:00"



class StatusStatSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    status = serializers.CharField()
    from_ = serializers.DateTimeField(format='%d-%m-%Y %H:%M:%S')
    to = serializers.DateTimeField(format='%d-%m-%Y %H:%M:%S')
    duration = serializers.IntegerField()


class URLSerializer(serializers.ModelSerializer):
    # stats = ResponseTimeSerializer(many=True)
    latest_status = serializers.CharField()

    class Meta:
        model = URL
        fields = ('id', 'url', 'latest_status')


class URLStatusSerializer(serializers.ModelSerializer):
    # last_status = serializers.SerializerMethodField()
    # last_status_code = serializers.SerializerMethodField()
    statistics = serializers.SerializerMethodField()

    class Meta:
        model = URL
        fields = ('id', 'url', 'last_status',
                  'last_status_code', 'statistics', )

    # def get_last_status(self, obj):
    #     last_status = StatusType(obj.last_status)
    #     return  last_status

    # def get_last_status_code(self, obj):
    #     last_status = obj.last_status
    #     return last_status

    def get_statistics(self, obj):
        last24 = datetime.now().astimezone() - timedelta(hours=24)
        queryset = URLStat.objects.filter(
            url__id=obj.id, created_at__gte=last24).order_by('created_at')

        if queryset:
            result = []
            prev = {}
            count = 0
            for stat in queryset:
                date = stat.created_at.astimezone()
                status_code = stat.status_code
                status = StatusType(status_code)

                if len(result) == 0:

                    duration = date - last24

                    prev = {'id': count, 'status': status, 'from': last24,
                            'to': date, 'duration': 0}
                    result.append(prev)
                    count += 1

                else:
                    if prev['status'] == status:
                        prev['to'] = date
                        duration = date - prev['from']
                        prev['duration'] = duration.total_seconds()//60
                        result[-1] = prev
                    else:
                        prev['to'] = date
                        duration = prev['to'] - prev['from']
                        prev['duration'] = duration.total_seconds()//60

                        result[-1] = prev

                        prev = {'id': count, 'status': status, 'from': date,
                                'to': date, 'duration': 0}
                        result.append(prev)
                        count += 1

            prev['to'] = datetime.now().astimezone()
            duration = prev['to'] - prev['from']
            prev['duration'] = duration.total_seconds()//60

            result[-1] = prev

            return result
        else:
            return None


class OrgSerializer(serializers.ModelSerializer):

    class Meta:
        model = Organization
        fields = ('id', 'name')


class DownSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    status_code = serializers.CharField()
    url = serializers.CharField(source='url__url')
    organization = serializers.SerializerMethodField()

    class Meta:
        model = URL
        # fields = ('id', 'url')
        fields = ('id', 'url', 'status', 'status_code',
                  'organization')

    def get_organization(self, obj):
        if obj['organization']:
            org = Organization.objects.filter(
                active=True).get(pk=obj['organization'])
            return OrgSerializer(org).data
        else:
            return None

    def get_status(self, obj):
        return StatusType(obj['status_code'])


class Down2Serializer(serializers.ModelSerializer):
    status_code = serializers.CharField()
    status = serializers.SerializerMethodField()
    organization = serializers.SerializerMethodField()

    class Meta:
        model = URL
        fields = ('id', 'url', 'status', 'status_code', 'organization')

    def get_status(self, obj):
        return StatusType(obj['status_code'])
    
    def get_organization(self, obj):
        if obj['organization']:
            org = Organization.objects.filter(
                active=True).get(pk=obj['organization'])
            if org:
                return {"id": org.id,
                        "name": org.name}
            else:
                return None
        else:
            return None