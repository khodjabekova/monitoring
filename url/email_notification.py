
from django.conf import settings
from django.core.mail import send_mail

from celery import shared_task
from celery.utils.log import get_task_logger


logger = get_task_logger(__name__)

@shared_task(serializer='json', name="send_mail")
def send_notification(email, url, status_code=None, log=None):
    if status_code:
        send_mail(
            "IT Monitoring",
            f"Notification about failure. URL {url} failed with status code {status_code}",
            settings.EMAIL_HOST_USER,
            [email],
            fail_silently=False,
        )
    else:
        send_mail(
            "IT Monitoring",
            f"Notification about failure. URL {url} failed. Log: {log}",
            settings.EMAIL_HOST_USER,
            [email],
            fail_silently=False,
        )