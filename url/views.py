from datetime import datetime, timedelta
import requests
from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils import timezone

from django.db.models.functions import Coalesce
from config.paginations import CustomPagination

from config.utils import StatusType
from department.models import Department
from . import serializers
from url.models import URLStat, URL
from organization.models import Organization
from information_system.models import ExpertiseInfo, InformationSystem
from web_site.models import WebSite
from mobileapp.models import MobileApp
from callcentre.models import Rating as CallCentreRating
from rating.models import Rating
from django.db.models import OuterRef, Subquery, Q, Count, F, Avg, Sum
from rest_framework import filters
from django.db.models.functions import TruncDate
from django.utils.translation import gettext_lazy as _


class URLListView(ListAPIView):
    """
    Barcha havolalar ro'yhati
    """
    serializer_class = serializers.URLListSerializer
    queryset = URL.objects.all()


class URLRatingView(ListAPIView):
    """
    front - mainpage
    back - /rating?ordering=-failed

    param:
        ordering:
            failed - Eng kam to‘xtalish bo‘lgan resurslar
            -failed - Eng ko‘p to‘xtalish bo‘lgan resurslar
    return:
        id - havola idsi
        url - havola
        failed - nechi marta ishlamagan
    """
    pagination_class = CustomPagination
    serializer_class = serializers.URLRaitingSerializer
    # filter_backends = [filters.OrderingFilter]
    # ordering_fields = ['failed', 'total_down']

    # def list(self, request, *args, **kwargs):
    #     self.pagination_class.page_size = 5
    #     return super().list(request, *args, **kwargs)

    # def get_queryset(self):
    #     # queryset = URL.objects.prefetch_related('stats').annotate(failed=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).count()))
    #     queryset = URL.objects.annotate(failed=Coalesce(Subquery(
    #         URLStat.objects.filter(url=OuterRef('pk'),)
    #         .filter(failed=True)
    #         .values('url')
    #         .annotate(count=Count('pk'))
    #         .values('count')), 0))

    #     return queryset

    def get_queryset(self):
        ordering = self.request.GET.get('ordering', 'failed')
        queryset = URLStat.objects.values('url__url').annotate(failed=Count('failed',filter=Q(failed=True)), total_down=Sum('down')).order_by(ordering)[:5]
        return queryset

# TODO: week stat return 7 respose, month return 30 response


class ResponseStatView(APIView):
    """
    Havolaning javob vaqti statistikasi
    front - /reestr/info/<organization_id>/ws/monitoring/<url_id>
    back - response/<pk>/<param>
    pk - havola idsi
    param:
        day - kunlik statistika
        week - haftalik statistika
        month - oylik statistika

    """

    def get(self, request, pk, param, format=None):
        # param = self.kwargs['region']
        if param == 'week':
            period = datetime.now().astimezone() - timedelta(days=7)
            queryset = URLStat.objects.filter(url_id=pk, created_at__gt=period)\
                .annotate(date=TruncDate('created_at'))\
                .values('date')\
                .annotate(response_time=Avg('response_time'))\
                .values('response_time', created_at=F('date'))\
                .order_by('created_at')
            # queryset = URLStat.objects.all()
            data = serializers.ResponseMonthWeekSerializer(
                queryset, many=True).data
            return Response(data)
        elif param == 'month':
            period = datetime.now().astimezone() - timedelta(days=30)
            queryset = URLStat.objects.filter(url_id=pk, created_at__gt=period)\
                .annotate(date=TruncDate('created_at'))\
                .values('date')\
                .annotate(response_time=Avg('response_time'))\
                .values('response_time', created_at=F('date'))\
                .order_by('created_at')
            # queryset = URLStat.objects.all()
            data = serializers.ResponseMonthWeekSerializer(
                queryset, many=True).data
            return Response(data)
        else:
            period = datetime.now().astimezone() - timedelta(days=1)
            queryset = URLStat.objects.filter(
                url_id=pk, created_at__gt=period).order_by('created_at')
            # queryset = URLStat.objects.all()
            data = serializers.ResponseTimeSerializer(queryset, many=True).data
            return Response(data)


class VisitorsStatView(APIView):
    """
    www uz hourly stat
    """

    def get(self, request, pk, format=None):

        hour = (datetime.now().astimezone() + timedelta(hours=1)).hour

        headers = {
            "Api-Security": 'Rj3ln9S4FoZqJfYVsCGj.wZtTnkmASvtgZner11j3.urQihkdmEYoQn86sJmTN'}

        lang = 'uz'
        try:
            id = URL.objects.get(pk=pk).www_id
            if id:
                date = datetime.now().astimezone().strftime("%Y-%m-%d")
                url = f"https://api.www.uz/api/www/hourly?lang={lang}&id={id}&date={date}"
                response = requests.get(url=url, headers=headers)
                json_response = response.json()
                data = json_response['data']['hourly']

                date2 = (datetime.now().astimezone() - timedelta(days=1)
                         ).astimezone().strftime("%Y-%m-%d")
                url2 = f"https://api.www.uz/api/www/hourly?lang={lang}&id={id}&date={date2}"
                response2 = requests.get(url=url2, headers=headers)
                json_response2 = response2.json()
                data2 = json_response2['data']['hourly']

                return Response(serializers.VisitorsSerializer(data2[hour:]+data, many=True).data)
            else:
                return Response([])
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST, exception=e)


class StatusStatView(ListAPIView):
    """
    Havolaning barcha statuslar royhatini qaytaradi

    front - reestr/info/<organization_id>/ws/monitoring/<url_id>
    back - status/<pk>
    param:
        pk - havola idsi

    return:
        id - tartib raqam
        status - status (active, failed, unknown)
        from_ - qachondan
        to - qachongacha
        duration - davomiyligi


    for each stat row in db, if previous status was same as current previous 'from' changed to current date,
    else current 'to' set to previous 'from' and add to list
    """
    pagination_class = CustomPagination
    serializer_class = serializers.StatusStatSerializer

    def get_queryset(self):
        pk = self.kwargs['pk']
        queryset = URLStat.objects.filter(url__id=pk).order_by('-created_at')

        result = []
        count = 0
        if queryset:
            prev = {}

            for stat in queryset:
                date = timezone.localtime(stat.created_at)
                status_code = stat.status_code
                status = StatusType(status_code)

                if len(result) == 0:
                    prev = {'id': count, 'status': status, 'from_': date,
                            'to':  datetime.now().astimezone(), 'duration': 0}
                    result.append(prev)
                    count += 1

                else:
                    if prev['status'] == status:
                        prev['from_'] = date
                        prev['duration'] = (
                            prev['to'] - prev['from_']).total_seconds()//60
                        result[-1] = prev
                    else:

                        duration = (prev['from_'] - date).total_seconds()//60
                        prev = {'id': count, 'status': status, 'from_': date,
                                'to': prev['from_'], 'duration': duration}
                        result.append(prev)
                        count += 1

        return result


class OrganizationsStatView(APIView):
    """
    Barcha departamentlar va tashkilotlar soni

    return:
        id - Departament idsi
        name - Departament nomi
        count - Tashkilotlar soni
    """

    def get(self, request, format=None):
        queryset = Department.objects.annotate(count=Count('sub_org')).order_by(
            'index').values('id', 'name', 'count')
        return Response(queryset)


class OrganizationsTotalCountView(APIView):
    """
    Barcha tashkilotlar soni
    """

    def get(self, request, format=None):
        count = Organization.objects.filter(active=True).count()
        return Response({'total_count': count})

from django.db import connection

class UpDownStatView(APIView):
    """
    Ishlab turgan va t
    return:
        at - Axbarot Tizimlar soni
        ws - Saytlar soni
        at_down - To'htalgan tizimlar soni
        ws_down - To'htalgan saytlar soni
        at_up - Ishlab turgan tizimlar soni
        ws_up - ishlab turgan saytlar soni

    """

    def get(self, request, format=None):
        at = InformationSystem.objects.filter(
            url__isnull=False, active=True).count()
        ws = WebSite.objects.filter(
            url__isnull=False, active=True, is_strategic=False).count()
        strategic = WebSite.objects.filter(
            url__isnull=False, active=True, is_strategic=True).count()

        # at_down = URL.objects.prefetch_related('stats').all()
        at_down = URL.objects.filter(information_system__isnull=False).prefetch_related('stats').annotate(stats_count=Count('stats'), latest_status=Subquery(
            URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__startswith='4') | Q(latest_status__startswith='5') | Q(stats_count__gte=1, latest_status__isnull=True)).count()
        ws_down = URL.objects.filter(website__isnull=False, website__is_strategic=False).prefetch_related('stats').annotate(stats_count=Count('stats'), latest_status=Subquery(
            URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__startswith='4') | Q(latest_status__startswith='5') | Q(stats_count__gte=1, latest_status__isnull=True)).count()
        strategic_down = URL.objects.filter(website__isnull=False, website__is_strategic=True).prefetch_related('stats').annotate(stats_count=Count('stats'), latest_status=Subquery(
            URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__startswith='4') | Q(latest_status__startswith='5') | Q(stats_count__gte=1, latest_status__isnull=True)).count()

        # at_down_count = at_down.filter(status_code__gte=400).count()

        # ws_down = URL.objects.filter(website__isnull=False).prefetch_related('stats').all()
        # return Response(serializers.URLSerializer(at_down, many=True).data)


        return Response({'at': at, 'at_down': at_down, 'at_up': at-at_down,
                         'ws': ws, 'ws_down': ws_down,  'ws_up': ws-ws_down,
                         'strategic': strategic, 'strategic_down': strategic_down,  'strategic_up': strategic - strategic_down})


class UpDown2StatView(APIView):
    """
    Ishlab turgan va t
    return:
        at - Axbarot Tizimlar soni
        ws - Saytlar soni
        at_down - To'htalgan tizimlar soni
        ws_down - To'htalgan saytlar soni
        at_up - Ishlab turgan tizimlar soni
        ws_up - ishlab turgan saytlar soni

    """

    def get(self, request, format=None):
        # at = InformationSystem.objects.filter(
        #     url__isnull=False, active=True).count()
        # ws = WebSite.objects.filter(
        #     url__isnull=False, active=True, is_strategic=False).count()
        # strategic = WebSite.objects.filter(
        #     url__isnull=False, active=True, is_strategic=True).count()

        # at_down = URL.objects.prefetch_related('stats').all()
        # at_down = URL.objects.filter(information_system__isnull=False).prefetch_related('stats').annotate(stats_count=Count('stats'), latest_status=Subquery(
        #     URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__startswith='4') | Q(latest_status__startswith='5') | Q(stats_count__gte=1, latest_status__isnull=True)).count()
        # ws_down = URL.objects.filter(website__isnull=False, website__is_strategic=False).prefetch_related('stats').annotate(stats_count=Count('stats'), latest_status=Subquery(
        #     URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__startswith='4') | Q(latest_status__startswith='5') | Q(stats_count__gte=1, latest_status__isnull=True)).count()
        # strategic_down = URL.objects.filter(website__isnull=False, website__is_strategic=True).prefetch_related('stats').annotate(stats_count=Count('stats'), latest_status=Subquery(
        #     URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__startswith='4') | Q(latest_status__startswith='5') | Q(stats_count__gte=1, latest_status__isnull=True)).count()

        # at_down_count = at_down.filter(status_code__gte=400).count()

        # ws_down = URL.objects.filter(website__isnull=False).prefetch_related('stats').all()
        # return Response(serializers.URLSerializer(at_down, many=True).data)

        with connection.cursor() as cursor:
            query = """
            select 
                (select count(*)
                FROM information_system
                where information_system.active=true 
                    and url_id is not NULL) as infosys,
                (select count(*)
                FROM organization_website
                where organization_website.active=true 
                    and organization_website.is_strategic=false
                    and url_id is not NULL) as ws,
                (select count(*)
                FROM organization_website
                where organization_website.active=true 
                    and organization_website.is_strategic=true
                    and url_id is not NULL) as strategic,

                (SELECT count(*)
                FROM information_system
                INNER JOIN url_stat ON information_system.url_id=url_stat.url_id
                where url_stat.id in (
                    SELECT MAX(id)
                    FROM url_stat
                    GROUP BY url_id ) 
                    and information_system.active=true 
                    and (CAST(url_stat.status_code as int) >= 400 or url_stat.status_code is NULL)) as is_active,

                (SELECT count(*)
                FROM organization_website
                INNER JOIN url_stat ON organization_website.url_id=url_stat.url_id
                where url_stat.id in (
                    SELECT MAX(id)
                    FROM url_stat
                    GROUP BY url_id ) 
                    and organization_website.active=true 
                    and organization_website.is_strategic=false
                    and (CAST(url_stat.status_code as int) >= 400 or url_stat.status_code is NULL)) as ws_active,

                (SELECT count(*)
                FROM organization_website
                INNER JOIN url_stat ON organization_website.url_id=url_stat.url_id
                where url_stat.id in (
                    SELECT MAX(id)
                    FROM url_stat
                    GROUP BY url_id ) 
                    and organization_website.active=true 
                    and organization_website.is_strategic=true
                    and (CAST(url_stat.status_code as int) >= 400 or url_stat.status_code is NULL)) as strategic_active
                ;
            """
            cursor.execute(query)
            row = cursor.fetchone()
            
            print(row)
            at = row[0]
            ws = row[1]
            strategic = row[2]
            # at_up = row[3]
            # ws_up = row[4]
            # strategic_up = row[5]
            at_down = row[3]
            ws_down = row[4]
            strategic_down = row[5]

        # return Response({'at': at, 'at_down': at - at_up, 'at_up': at_up,
        #                  'ws': ws, 'ws_down': ws- ws_up,  'ws_up': ws_up,
        #                  'strategic': strategic, 'strategic_down': strategic - strategic_up,  'strategic_up': strategic_up})

        return Response({'at': at, 'at_down': at_down, 'at_up': at-at_down,
                         'ws': ws, 'ws_down': ws_down,  'ws_up': ws - ws_down,
                         'strategic': strategic, 'strategic_down': strategic_down,  'strategic_up': strategic - strategic_down})


class DownStatView(ListAPIView):
    # serializer_class = serializers.DownSerializer
    serializer_class = serializers.Down2Serializer
    pagination_class = CustomPagination

    # def get_queryset(self):
    #     queryset = URL.objects.prefetch_related('stats')\
    #         .annotate(stats_count=Count('stats'), down_time=Subquery(URLStat.objects.filter(url=OuterRef('pk'),).order_by('-created_at').values('down')[:1]),
    #                   status_code=Subquery(URLStat.objects.filter(url=OuterRef(
    #                       'pk'),).order_by('-created_at').values('status_code')[:1]),
    #                   active=Coalesce('website__organization__active', 'information_system__organization__active'))\
    #         .filter((Q(status_code__startswith='4') | Q(status_code__startswith='5') | Q(status_code__isnull=True)), stats_count__gte=1)\
    #         .values('id', 'url', 'status_code', 'down_time', 'active', org_id=Coalesce('website__organization', 'information_system__organization')).filter(active=True)
    #     return queryset
    # def get_queryset(self):
    #     queryset1 = WebSite.objects.filter(active=True, is_strategic=False).select_related('url')\
    #         .annotate(status_code=Subquery(URLStat.objects.filter(url=OuterRef('url__id'),).order_by('-created_at').values('status_code')[:1]))\
    #         .filter((Q(status_code__startswith='4') | Q(status_code__startswith='5') | Q(status_code__isnull=True)))\
    #         .values('id', 'status_code', 'organization', 'url__url')
    #     queryset2 = InformationSystem.objects.filter(active=True).select_related('url')\
    #         .annotate(status_code=Subquery(URLStat.objects.filter(url=OuterRef('url__id'),).order_by('-created_at').values('status_code')[:1]))\
    #         .filter((Q(status_code__startswith='4') | Q(status_code__startswith='5') | Q(status_code__isnull=True)))\
    #         .values('id', 'status_code', 'organization', 'url__url')
    #     queryset = queryset1.union(queryset2)
    #     return queryset
    def get_queryset(self):
        queryset = URL.objects.prefetch_related('website', 'information_system')\
            .annotate(status_code=Subquery(URLStat.objects.filter(url=OuterRef(
                            'pk'),).order_by('-created_at').values('status_code')[:1]),
                        active=Coalesce('website__organization__active', 'information_system__organization__active'))\
            .filter((Q(status_code__startswith='4') | Q(status_code__startswith='5') | Q(status_code__isnull=True)))\
            .values('id', 'url', 'status_code', 'active', organization=Coalesce('website__organization', 'information_system__organization')).filter(active=True)
        return queryset
class DownStat2View(ListAPIView):
    serializer_class = serializers.Down2Serializer
    pagination_class = CustomPagination

    def list(self, request, *args, **kwargs):
        self.pagination_class.page_size = 5
        return super().list(request, *args, **kwargs)
  
    def get_queryset(self):
        queryset = URL.objects.prefetch_related('website', 'information_system')\
            .annotate(status_code=Subquery(URLStat.objects.filter(url=OuterRef(
                          'pk'),).order_by('-created_at').values('status_code')[:1]),
                      active=Coalesce('website__organization__active', 'information_system__organization__active'))\
            .filter((Q(status_code__startswith='4') | Q(status_code__startswith='5') | Q(status_code__isnull=True)))\
            .values('id', 'url', 'status_code', 'active', organization=Coalesce('website__organization', 'information_system__organization')).filter(active=True)
        return queryset
    

class LocationStatView(APIView):

    def get(self, request, format=None):
        total = InformationSystem.objects.filter(active=True).count()
        inside = InformationSystem.objects.filter(
            location=1, active=True).count()
        outside = InformationSystem.objects.filter(
            location=2, active=True).count()

        result = [{'label': InformationSystem.LOCAION_CHOICE[0][1], "total": inside, 'percent': round((inside/total)*100)},
                  {'label': InformationSystem.LOCAION_CHOICE[1][1], "total": outside, 'percent': round((outside/total)*100)}]
        return Response(result)


class ExpertStatView(APIView):

    def get(self, request, format=None):
        insys = InformationSystem.objects.filter(active=True).count()
        digital_expert = ExpertiseInfo.objects.filter(
            information_system__active=True, digital_expert=1).count()
        csec_expert = ExpertiseInfo.objects.filter(
            information_system__active=True, csec_expert=1).count()
        tech_assignment = ExpertiseInfo.objects.filter(
            information_system__active=True, tech_assignment=1).count()
        cybersecurity_expert = ExpertiseInfo.objects.filter(
            information_system__active=True, cybersecurity_expert=1).count()

        data = [
            {'label': ExpertiseInfo.EXPERT_CHOICE[1], 'total':digital_expert, 'percent': round(
                (digital_expert/insys)*100)},
            {'label': ExpertiseInfo.EXPERT_CHOICE[2], 'total':csec_expert, 'percent': round(
                (csec_expert/insys)*100)},
            {'label': ExpertiseInfo.EXPERT_CHOICE[0], 'total':tech_assignment, 'percent': round(
                (tech_assignment/insys)*100)},
            {'label': ExpertiseInfo.EXPERT_CHOICE[3], 'total':cybersecurity_expert, 'percent': round((cybersecurity_expert/insys)*100)}]
        return Response(data)

        # return Response({'digital_expert': digital_expert,
        #                  'csec_expert': csec_expert,
        #                  'tech_assignment': tech_assignment,
        #                  'cybersecurity_expert': cybersecurity_expert})


class ProtocolStatView(APIView):

    def get(self, request, format=None):
        ws = WebSite.objects.filter(active=True).count()
        correct = WebSite.objects.filter(
            ssl_certificate_exist=1, active=True).count()
        incorrect = WebSite.objects.filter(
            ssl_certificate_exist=3, active=True).count()
        not_exist = ws-correct-incorrect

        data = [{'label': WebSite.CHOICE1[0][1], 'total':correct, 'percent': round((correct/ws)*100)},
                {'label': WebSite.CHOICE1[1][1], 'total':not_exist, 'percent': round(
                    (not_exist/ws)*100)},
                {'label': WebSite.CHOICE1[2][1], 'total':incorrect, 'percent': round((incorrect/ws)*100)}]
        return Response(data)


class MobileAppStatView(APIView):

    def get(self, request, format=None):
        total = MobileApp.objects.count()
        android = MobileApp.objects.filter(url_android__isnull=False).count()
        iOS = MobileApp.objects.filter(url_ios__isnull=False).count()

        data = [{'label': 'Android', 'total': android, 'percent': round((android/total)*100)},
                {'label': 'iOS', 'total': iOS, 'percent': round((iOS/total)*100)}]
        return Response(data)

# TODO: change current year and quarter


class CallCentreStatView(APIView):

    def get(self, request, format=None):
        # current_year = datetime.today().year
        # current_quarter = (datetime.today().month - 1 )// 3 + 1
        current_year = 2022
        current_quarter = 3
        total = CallCentreRating.objects.filter(
            year=current_year, quarter=current_quarter).count()
        good = CallCentreRating.objects.filter(year=current_year, quarter=current_quarter).annotate(total=F('response_rate')+F('attempts_rate')+F('avrg_time_rate')+F('working_hours_rate')+F('feedback_rate')+F('telegram_bot_rate')+F('behavior_rate'))\
            .filter(total__gte=85).count()
        bad = CallCentreRating.objects.filter(year=current_year, quarter=current_quarter).annotate(total=F('response_rate')+F('attempts_rate')+F('avrg_time_rate')+F('working_hours_rate')+F('feedback_rate')+F('telegram_bot_rate')+F('behavior_rate'))\
            .filter(total__lte=69).count()
        satisfy = total - good - bad

        data = [{'label': _('Yaxshi (85-100)'), 'total': good},
                {'label': _('Qoniqarli (70-84)'), 'total': satisfy},
                {'label': _('Qoniqarsiz (0-69)'), 'total': bad}]
        return Response(data)


class OrganizationRatingStatView(APIView):

    def get(self, request, format=None):
        total = Rating.objects.count()
        good = Rating.objects.annotate(percent=(
            ((F('collected') - F('deducted'))/F('max_score'))*100)).filter(percent__gte=71).count()
        bad = Rating.objects.annotate(percent=(
            ((F('collected') - F('deducted'))/F('max_score'))*100)).filter(percent__lte=54).count()
        satisfy = total - good - bad
        data = [{'label': _('Yaxshi (71-100)'), 'total': good},
                {'label': _('Qoniqarli (55-70)'), 'total': satisfy},
                {'label': _('Qoniqarsiz (0-54)'), 'total': bad}]
        return Response(data)


class UrlStatView(APIView):
    """
    /reestr/info/<organization_id>/ws/monitoring/<url_id>

    url - havola
    content_manager - Mas’ul xodim
    phone - Mas’ul xodimning telefon raqami
    status - ohirgi status
    checked - Monitoring qilinayotgan vaqt
    last_checked - So‘nggi tekshirish
    failed_count - Sodir bo‘lgan hodisalar
    total_down - Ummumiy uzilish vaqti
    last_failed - So‘nggi uzilish vaqti
    """

    def get(self, request, pk, format=None):
        try:

            obj = URL.objects.prefetch_related(
                'website', 'information_system').get(id=pk)
            obj_stat = URLStat.objects.values('created_at').filter(url_id=pk)

            if obj_stat:
                checked_from = obj_stat.earliest('created_at')['created_at']
                duration = (datetime.now().astimezone() -
                            checked_from).total_seconds() // 60
                last_checked = obj_stat.latest('created_at')['created_at']
                last_checked_ago = (datetime.now().astimezone(
                ) - last_checked).total_seconds() // 60
                failed = obj_stat.filter(failed=True)

                if failed:
                    failed_time = failed.latest('created_at')['created_at']
                    last_failed = timezone.localtime(
                        failed_time).strftime("%d-%m-%Y %H:%M:%S")
                    failed_count = failed.count()
                else:
                    last_failed = None
                    failed_count = None

                status_type = obj.last_status
            else:
                checked_from = None
                duration = None
                last_checked_ago = None
                failed_count = None
                last_failed = None
                status_type = None

            if obj.website:
                content_manager = obj.website.content_manager
                phone = obj.website.cm_phone
            else:
                content_manager = obj.information_system.admin
                phone = obj.information_system.admin_phone

            data = {'url': obj.url,
                    'content_manager': content_manager,
                    'phone': phone,
                    'status': status_type,
                    'checked': duration,
                    'last_checked': last_checked_ago,
                    'failed_count': failed_count,
                    'total_down': obj.total_down,
                    'last_failed': last_failed}

            return Response(data)
        except Exception as e:
            return Response(status=status.HTTP_404_NOT_FOUND, exception=e)
