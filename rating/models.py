from django.db import models
from django.utils.translation import gettext_lazy as _

from baseapp.models import BaseModel
from organization.models import Organization


class Rating(BaseModel):
    organization = models.ForeignKey(
        Organization, on_delete=models.CASCADE, related_name='rating', verbose_name='Tashkilot nomi')
    max_score = models.FloatField(default=0, verbose_name='Belgilangan ball')
    collected = models.FloatField(default=0, verbose_name='Toʻplangan ball')
    deducted = models.FloatField(
        default=0, verbose_name='Olib tashlangan ball')

    @property
    def final_score(self):
        final_score = self.collected - self.deducted
        return final_score
      
    
    final_score.fget.short_description = "Yakuniy ball"

    @property
    def percent(self):
        if self.max_score and self.max_score > 0:
            percent = ((self.collected - self.deducted)/self.max_score)*100
            return f'{percent:.2f} %'
        return '0 %'
    percent.fget.short_description = "Foizda %"

    def __str__(self):
        return self.organization.name

    class Meta:
        db_table = 'rating'
        verbose_name = _("Reyting")
        verbose_name_plural = _("Reyting")
