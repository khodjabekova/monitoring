from django.urls import path

from . import views

urlpatterns = [
    path('', views.RatingListView.as_view()),
]
