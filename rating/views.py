from rest_framework.generics import ListAPIView
from config.paginations import CustomPagination
from config.search import get_query
from . import serializers
from .models import Rating
from rest_framework import filters
from django.db.models import F
from django.utils.translation import get_language

class RatingListView(ListAPIView):
    pagination_class = CustomPagination
    serializer_class = serializers.OrganizationRatingListSerializer
    # queryset = Rating.objects.annotate(score=F('collected')-F('deducted'))
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['score']

    def get_queryset(self):
        request = self.request
        q = request.GET.get('search', '')
        lang = get_language()
        param1 = f'organization__name_{lang}'
        if q:
            entry_query = get_query(q, [param1])
            queryset = Rating.objects.annotate(score=F('collected')-F('deducted')).filter(entry_query)

        else:
            queryset = Rating.objects.annotate(score=F('collected')-F('deducted'))
        return queryset