from django.contrib import admin
from .models import Rating


@admin.register(Rating)
class RatingAdmin(admin.ModelAdmin):
    list_display = ('organization', 'max_score', 'collected',
                    'deducted', 'final_score', 'percent')
    list_filter = ('organization', )
    search_fields = ['organization__name',]
    exclude = ('created_by', 'updated_by',)
