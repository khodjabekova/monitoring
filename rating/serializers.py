from .models import Rating
from rest_framework import serializers
from url.models import URL
from web_site.models import WebSite


class OrganizationRatingListSerializer(serializers.ModelSerializer):
    organization = serializers.SlugRelatedField(
        slug_field='name', read_only=True)
    web_site = serializers.SerializerMethodField()

    class Meta:
        model = Rating
        fields = ('id', 'organization', 'web_site', 'max_score',
                  'collected', 'deducted', 'final_score', 'percent')

    def get_web_site(self, obj):
        if obj.organization:
            try:
                # ws = obj.organization.websites.filter(is_main=True).first()
                ws = WebSite.objects.select_related('url').filter(organization=obj.organization,is_main=True, active=True).values('url__url').first()
                return ws['url__url']
            except:
                return None
        return None
