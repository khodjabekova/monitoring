from django.db import models
from django.utils.translation import gettext_lazy as _
from baseapp.models import BaseModel
from organization.models import Organization

from django.core.validators import MaxValueValidator, MinValueValidator


class CallCentre(BaseModel):
    organization = models.ForeignKey(Organization, on_delete=models.SET_NULL, null=True,
                                     blank=True, verbose_name=_('Davlat organlari va tashkilotlarning nomi'))
    phone = models.CharField(max_length=12, verbose_name=_(
        'Idoraviy aloqa markazlari (ishonch telefonlari) raqamlari'))
    short_phone = models.CharField(
        max_length=4, null=True, blank=True, verbose_name=_('Qisqa raqam'))
    
    technology = models.CharField(
        max_length=100, null=True, blank=True, verbose_name=_('Aloqani tashkil etish texnologiyasi'))
    channels = models.IntegerField(
        null=True, blank=True, verbose_name=_('Аjratilgan telefon kanallari soni'))

    # Chorak davomida telefon raqamlaridagi (kanallardagi) nosozliklar
    failures = models.IntegerField(null=True, blank=True, verbose_name=_(
        'Chorak davomida telefon raqamlaridagi (kanallardagi) nosozliklar '))
    stops = models.IntegerField(
        null=True, blank=True, verbose_name=_('Shundan to`xtashlar soni'))
    duration = models.IntegerField(
        null=True, blank=True, verbose_name=_('To`xtashlar davomiyligi'))
    
    employee = models.IntegerField(null=True, blank=True, verbose_name=_(
        'Idoraviy aloqa markazi operatorlari soni'))

    # Xodimlarning ma’lumoti va malaka oshirganligi
    higher_empl = models.IntegerField(
        null=True, blank=True, verbose_name=_('Oliy ma’lumot soni'))
    secondary_empl = models.IntegerField(
        null=True, blank=True, verbose_name=_('O‘rta ma’lumot soni'))
    qualified_empl = models.IntegerField(
        null=True, blank=True, verbose_name=_('Malaka oshirgan soni'))

    disciplined_empl = models.IntegerField(null=True, blank=True, verbose_name=_(
        'Murojaatlar bilan ishlashda yo‘l qo‘yilgan qoidabuzish sababli chora ko‘rilgan xodimlar soni'))

    # Xodimlarning taqsimlangan ish vaqtida
    incoming_calls = models.CharField(max_length=12, null=True, blank=True, verbose_name=_(
        'Liniyaga kelib tushgan qo‘ng‘iroqlar soni'))
    received_calls = models.IntegerField(
        null=True, blank=True, verbose_name=_('Qabul qilingan qo‘ng‘iroqlar soni'))

    comment = models.CharField(
        max_length=100, null=True, blank=True, verbose_name=_('Izoh*'))

    def __str__(self):
        return f'{self.organization} ({self.phone})'

    class Meta:
        db_table = 'call_centre'
        verbose_name = _("Call Centre")
        verbose_name_plural = _("Call Centres")


class Period(models.Model):
    call_centre = models.ForeignKey(
        CallCentre, on_delete=models.CASCADE, related_name='periods')
    # Ish vaqtida xodimlarning taqsimlanishi
    time_interval = models.CharField(
        max_length=11, verbose_name=_('Vaqt oralig‘i'))
    employee_amount = models.IntegerField(
        null=True, blank=True, verbose_name=_('Xodim soni'))

    class Meta:
        db_table = 'call_centre_period'
        verbose_name = _("Ish vaqtida xodimlarning taqsimlanishi")
        verbose_name_plural = _("Ish vaqtida xodimlarning taqsimlanishi")


class Rating(BaseModel):
    AVAILABLE = 1
    NOT_AVAILABLE = 2

    CHOICE = (
        (AVAILABLE, _('Mavjud')),
        (NOT_AVAILABLE, _('Mavjud emas')),
    )

    QUARTER_CHOICE = (
        (1, 'I'),
        (2, 'II'),
        (3, 'III'),
        (4, 'IV'),
    )

    call_centre = models.ForeignKey(
        CallCentre, on_delete=models.CASCADE, related_name='ratings')

    year = models.IntegerField(validators=[MinValueValidator(
        1900), MaxValueValidator(2100)], verbose_name=_('Yil'))
    quarter = models.PositiveSmallIntegerField(
        choices=QUARTER_CHOICE, verbose_name=_('Chorak'))

    response_note = models.CharField(max_length=255, null=True, blank=True, verbose_name=_(
        'Javob berilganlik darajasi (tavsif)'))
    response_rate = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(
        20)], null=True, blank=True, verbose_name=_('ball (max 20)'))

    attempts_note = models.CharField(max_length=255, null=True, blank=True, verbose_name=_(
        'Operator bilan bogʻlanishga ketgan urinishlar soni (tavsif)'))
    attempts_rate = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(
        20)], null=True, blank=True, verbose_name=_('ball (max 20)'))

    avrg_time_note = models.CharField(max_length=255, null=True, blank=True, verbose_name=_(
        'Operator bilan bogʻlanish uchun ketgan oʻrtacha vaqti (tavsif)'))
    avrg_time_rate = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(
        20)], null=True, blank=True, verbose_name=_('ball (max 20)'))

    working_hours_note = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_('Ish tartibi (tavsif)'))
    working_hours_rate = models.IntegerField(validators=[MinValueValidator(
        0), MaxValueValidator(10)], null=True, blank=True, verbose_name=_('ball (max 10)'))

    feedback_note = models.PositiveSmallIntegerField(
        choices=CHOICE, null=True, blank=True, verbose_name=_('Qayta aloqa xizmati (tavsif)'))
    feedback_rate = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(
        10)], null=True, blank=True, verbose_name=_('ball (max 10)'))

    telegram_bot_note = models.PositiveSmallIntegerField(
        choices=CHOICE, null=True, blank=True, verbose_name=_('Telegram Bot (tavsif)'))
    telegram_bot_rate = models.IntegerField(validators=[MinValueValidator(
        0), MaxValueValidator(10)], null=True, blank=True, verbose_name=_('ball (max 10)'))

    # behavior_note = models.CharField(max_length=255, null=True, blank=True, verbose_name=_(
    #     'Operatorning muomala madaniyati (tavsif)'))
    behavior_rate = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(
        10)], null=True, blank=True, verbose_name=_('Operatorning muomala madaniyati (tavsif)'))

    @property
    def total(self):
        p1 = self.response_rate if self.response_rate else 0
        p2 = self.attempts_rate if self.attempts_rate else 0
        p3 = self.avrg_time_rate if self.avrg_time_rate else 0
        p4 = self.working_hours_rate if self.working_hours_rate else 0
        p5 = self.feedback_rate if self.feedback_rate else 0
        p6 = self.telegram_bot_rate if self.telegram_bot_rate else 0
        p7 = self.behavior_rate if self.behavior_rate else 0

        total = p1 + p2 + p3 + p4 + p5 + p6 + p7
        # total = self.response_rate + self.attempts_rate + self.avrg_time_rate + \
        #     self.working_hours_rate + self.feedback_rate + \
        #     self.telegram_bot_rate + self.behavior_rate
        return total

    def __str__(self):
        return f'{self.year}-{self.quarter}'

    class Meta:
        ordering = ['year', 'quarter']
        db_table = 'call_centre_rating'
        verbose_name = _("Call Centre Rating")
        verbose_name_plural = _("Call Centre Rating")


"""
Davlat organlari va tashkilotlarning idoraviy aloqa markazlari ishonch telefonlariga tushgan murojaatlar bo‘yicha hisobot
"""


class CallStatistics(models.Model):
    QUARTER_CHOICE = (
        (1, 'I'),
        (2, 'II'),
        (3, 'III'),
        (4, 'IV'),
    )

    call_centre = models.ForeignKey(
        CallCentre, on_delete=models.CASCADE, related_name='stats')

    year = models.IntegerField(validators=[MinValueValidator(
        1900), MaxValueValidator(2100)], verbose_name=_('Yil'))
    quarter = models.PositiveSmallIntegerField(
        choices=QUARTER_CHOICE, verbose_name=_('Chorak'))

    incomings = models.IntegerField(null=True, blank=True, verbose_name=_(
        'Liniyaga kelib tushgan qo‘ng‘iroqlar (ta)'))
    recieved = models.IntegerField(
        null=True, blank=True, verbose_name=_('Qabul qilingan qo‘ng‘iroqlar (ta)'))
    conv_duration_total = models.CharField(max_length=100, null=True, blank=True, verbose_name=_(
        'Suhbatning umumiy davomiyligi (daq soniya)'))
    conv_duration_average = models.CharField(max_length=100, null=True, blank=True, verbose_name=_(
        'Suhbatning o‘rtacha davomiyligi (daq soniya)'))
    outgoing = models.IntegerField(
        null=True, blank=True, verbose_name=_('Chiquvchi qo‘ng‘iroqlar (ta)'))

    @property
    def percent(self):
        if self.recieved and self.incomings and self.incomings > 0:
            percent = (self.recieved / self.incomings) * 100
            return f'{percent:.2f}'
        else:
            return 0

    @property
    def missed(self):
        if self.recieved and self.incomings:
            missed = self.incomings - self.recieved
            return missed
        else:
            return 0

    class Meta:
        db_table = 'call_centre_stats'
        verbose_name = _("Qo'ng'iroqlar bo'yicha ma‘lumotlar")
        verbose_name_plural = _("Qo'ng'iroqlar bo'yicha ma‘lumotlar")


class ActivityStatistics(models.Model):

    CHOICE1 = (
        (1, _('Mavjud')),
        (2, _('Mavjud emas')),
    )

    CHOICE2 = (
        (1, _('Joylashtirilgan')),
        (2, _('Joylashtirilmagan')),
    )

    CHOICE3 = (
        (1, _('Saqlanadi')),
        (2, _('Saqlanmaydi')),
    )

    QUARTER_CHOICE = (
        (1, 'I'),
        (2, 'II'),
        (3, 'III'),
        (4, 'IV'),
    )

    call_centre = models.ForeignKey(
        CallCentre, on_delete=models.CASCADE, related_name='activity')

    year = models.IntegerField(validators=[MinValueValidator(
        1900), MaxValueValidator(2100)], verbose_name=_('Yil'))
    quarter = models.PositiveSmallIntegerField(
        choices=QUARTER_CHOICE, verbose_name=_('Chorak'))

    working_hours = models.CharField(max_length=255, null=True, blank=True, verbose_name=_(
        'Idoraviy aloqa markazlari (ishonch telefonlari) ish rejimi'))
    feedback_note = models.PositiveSmallIntegerField(
        choices=CHOICE1, null=True, blank=True, verbose_name=_('Qayta aloqa xizmatining mavjudligi'))
    telegram_bot_note = models.PositiveSmallIntegerField(
        choices=CHOICE1, null=True, blank=True, verbose_name=_('Murojaatlar bilan ishlashda “Telegram bot” mavjudligi'))
    hotline = models.PositiveSmallIntegerField(
        choices=CHOICE2, null=True, blank=True, verbose_name=_('Ishonch telefon raqamlarining rasmiy veb-saytlarida joylashtirilishi'))
    save_phone = models.PositiveSmallIntegerField(
        choices=CHOICE3, null=True, blank=True, verbose_name=_('Telefon so‘zlashuvlari (kamida ikki oy mobaynida) saqlanishi'))
    save_application = models.PositiveSmallIntegerField(
        choices=CHOICE3, null=True, blank=True, verbose_name=_('Murojaat matnlari avtomatlashtirilgan axborot tizimida  (kamida ikki yil mobaynida) saqlanishi'))

    class Meta:
        db_table = 'call_centre_activity_stats'
        verbose_name = _("Faoliyat bo'yicha ma‘lumot")
        verbose_name_plural = _("Faoliyat bo'yicha ma‘lumot")
