# Generated by Django 3.2.4 on 2023-03-13 11:31

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('callcentre', '0002_rating'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rating',
            name='attempts_rate',
            field=models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(20)], verbose_name='Operator bilan bogʻlanishga ketgan urinishlar soni (ball)'),
        ),
        migrations.AlterField(
            model_name='rating',
            name='avrg_time_rate',
            field=models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(20)], verbose_name='Operator bilan bogʻlanish uchun ketgan oʻrtacha vaqti (ball)'),
        ),
        migrations.AlterField(
            model_name='rating',
            name='behavior_rate',
            field=models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(10)], verbose_name='Operatorning muomala madaniyati (ball)'),
        ),
        migrations.AlterField(
            model_name='rating',
            name='feedback_rate',
            field=models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(10)], verbose_name='Qayta aloqa xizmati (ball)'),
        ),
        migrations.AlterField(
            model_name='rating',
            name='response_rate',
            field=models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(20)], verbose_name='Javob berilganlik darajasi (ball)'),
        ),
        migrations.AlterField(
            model_name='rating',
            name='telegram_bot_rate',
            field=models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(10)], verbose_name='Telegram Bot (ball)'),
        ),
        migrations.AlterField(
            model_name='rating',
            name='working_hours_rate',
            field=models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(10)], verbose_name='Ish tartibi (ball)'),
        ),
    ]
