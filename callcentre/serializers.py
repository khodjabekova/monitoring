from organization.models import Organization
from .models import ActivityStatistics, CallCentre, CallStatistics, Period, Rating as CallCentreRating
from rest_framework import serializers


class CallCentreListSerializer(serializers.ModelSerializer):
    organization = serializers.SlugRelatedField(
        slug_field='name', read_only=True)

    class Meta:
        model = CallCentre
        fields = ('id', 'phone', 'organization', )


class CallCentreOrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = ('id', 'name', )


class PeriodListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Period
        fields = ('time_interval', 'employee_amount', )


class CallCentreRatingListSerializer(serializers.ModelSerializer):
    organization = serializers.SerializerMethodField()
    phone = serializers.SerializerMethodField()

    class Meta:
        model = CallCentreRating
        fields = ('id', 'organization', 'phone', 'response_note', 'response_rate', 'attempts_note', 'attempts_rate',
                  'avrg_time_note', 'avrg_time_rate', 'working_hours_note', 'working_hours_rate', 'feedback_note',
                  'feedback_rate', 'telegram_bot_note', 'telegram_bot_rate', 'behavior_rate', 'total')

    def get_phone(self, obj):
        if obj.call_centre.phone:
            return obj.call_centre.phone
        else:
            return None

    def get_organization(self, obj):
        if obj.call_centre and obj.call_centre.organization:
            return CallCentreOrganizationSerializer(obj.call_centre.organization).data
        else:
            return None


class CallCentreWorkSerializer(serializers.ModelSerializer):
    organization = serializers.SerializerMethodField()

    class Meta:
        model = CallCentre
        fields = ('organization', 'phone', 'short_phone', 'technology',
                  'channels', 'failures', 'stops', 'duration', 'comment',)

    def get_organization(self, obj):
        if obj.organization:
            return CallCentreOrganizationSerializer(obj.organization).data
        else:
            return None


class CallCentreCallsInfoListSerializer(serializers.ModelSerializer):
    organization = serializers.SerializerMethodField()
    phone = serializers.SerializerMethodField()

    class Meta:
        model = CallStatistics
        fields = ('id', 'organization', 'phone', 'incomings', 'recieved', 'missed',
                  'conv_duration_total', 'conv_duration_average', 'outgoing', 'percent')

    def get_phone(self, obj):
        if obj.call_centre.phone:
            return obj.call_centre.phone
        else:
            return None

    def get_organization(self, obj):
        if obj.call_centre and obj.call_centre.organization:
            return CallCentreOrganizationSerializer(obj.call_centre.organization).data
        else:
            return None


class CallCentreOperatorSerializer(serializers.ModelSerializer):
    organization = serializers.SerializerMethodField()
    periods = PeriodListSerializer(many=True)

    class Meta:
        model = CallCentre
        fields = ('organization', 'phone', 'employee', 'higher_empl', 'secondary_empl',
                  'qualified_empl', 'disciplined_empl', 'incoming_calls', 'received_calls', 'periods')

    def get_organization(self, obj):
        if obj.organization:
            return CallCentreOrganizationSerializer(obj.organization).data
        else:
            return None


class CallCentreActivityListSerializer(serializers.ModelSerializer):
    organization = serializers.SerializerMethodField()
    telegram_bot_note = serializers.CharField(
        source='get_telegram_bot_note_display', read_only=True)
    feedback_note = serializers.CharField(
        source='get_feedback_note_display', read_only=True)
    hotline = serializers.CharField(
        source='get_hotline_display', read_only=True)
    save_phone = serializers.CharField(
        source='get_save_phone_display', read_only=True)
    save_application = serializers.CharField(
        source='get_save_application_display', read_only=True)
    phone = serializers.SerializerMethodField()

    class Meta:
        model = ActivityStatistics
        fields = ('organization', 'phone', 'working_hours', 'feedback_note',
                  'telegram_bot_note', 'hotline', 'save_phone', 'save_application',)

    def get_phone(self, obj):
        if obj.call_centre.phone:
            return obj.call_centre.phone
        else:
            return None

    def get_organization(self, obj):
        if obj.call_centre and obj.call_centre.organization:
            return CallCentreOrganizationSerializer(obj.call_centre.organization).data
        else:
            return None


class RatingListSerializer(serializers.ModelSerializer):
    quarter = serializers.CharField(
        source='get_quarter_display', read_only=True)
    telegram_bot_note = serializers.CharField(
        source='get_telegram_bot_note_display', read_only=True)
    feedback_note = serializers.CharField(
        source='get_feedback_note_display', read_only=True)

    class Meta:
        model = CallCentreRating
        fields = ('year', 'quarter', 'response_note', 'response_rate', 'attempts_note', 'attempts_rate',
                  'avrg_time_note', 'avrg_time_rate', 'working_hours_note', 'working_hours_rate', 'feedback_note',
                  'feedback_rate', 'telegram_bot_note', 'telegram_bot_rate', 'behavior_rate', 'total',)


class CallStatisticsSerializer(serializers.ModelSerializer):
    quarter = serializers.CharField(
        source='get_quarter_display', read_only=True)

    class Meta:
        model = CallStatistics
        fields = ('year', 'quarter', 'incomings', 'recieved', 'conv_duration_total',
                  'conv_duration_average', 'outgoing', 'percent', 'missed',)


class ActivityStatisticsSerializer(serializers.ModelSerializer):
    quarter = serializers.CharField(
        source='get_quarter_display', read_only=True)
    telegram_bot_note = serializers.CharField(
        source='get_telegram_bot_note_display', read_only=True)
    feedback_note = serializers.CharField(
        source='get_feedback_note_display', read_only=True)
    hotline = serializers.CharField(
        source='get_hotline_display', read_only=True)
    save_phone = serializers.CharField(
        source='get_save_phone_display', read_only=True)
    save_application = serializers.CharField(
        source='get_save_application_display', read_only=True)

    class Meta:
        model = ActivityStatistics
        fields = ('year', 'quarter', 'working_hours', 'feedback_note', 'telegram_bot_note',
                  'hotline', 'save_phone', 'save_application',)


class CallCentreDetailsSerializer(serializers.ModelSerializer):
    organization = serializers.SlugRelatedField(
        slug_field='name', read_only=True)
    periods = PeriodListSerializer(many=True)
    ratings = RatingListSerializer(many=True)
    stats = CallStatisticsSerializer(many=True)
    activity = ActivityStatisticsSerializer(many=True)

    class Meta:
        model = CallCentre
        fields = ('organization', 'phone', 'short_phone', 'employee', 'higher_empl', 'secondary_empl', 'qualified_empl',
                  'disciplined_empl', 'incoming_calls', 'received_calls', 'periods', 'ratings', 'stats', 'activity',)
