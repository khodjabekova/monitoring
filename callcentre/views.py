from rest_framework.generics import ListAPIView, RetrieveAPIView
from config.paginations import CustomPagination
from . import serializers
from .models import CallCentre, Rating as CallCentreRating, CallStatistics, ActivityStatistics
from rest_framework import filters
from django.db.models import F
from django.db.models import F, Case, When
from config.search import get_query
from django.utils.translation import get_language


class CallCentreListView(ListAPIView):
    pagination_class = CustomPagination
    serializer_class = serializers.CallCentreListSerializer
    queryset = CallCentre.objects.all()


class CallCentreRatingListView(ListAPIView):
    pagination_class = CustomPagination
    serializer_class = serializers.CallCentreRatingListSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['score']

    def get_queryset(self):
        request = self.request
        q = request.GET.get('search', '')
        lang = get_language()
        param = f'call_centre__organization__name_{lang}'

        current_year = 2022
        current_quarter = 3
        year = int(self.request.GET.get('year', current_year))
        quarter = int(self.request.GET.get('quarter', current_quarter))
        if q:
            entry_query = get_query(q, [param])
            queryset = CallCentreRating.objects.select_related('call_centre').filter(year=year, quarter=quarter)\
                .annotate(score=F('response_rate') + F('attempts_rate') +
                          F('avrg_time_rate') + F('working_hours_rate') + F('feedback_rate') +
                          F('telegram_bot_rate') + F('behavior_rate')).filter(entry_query)
        else:
            queryset = CallCentreRating.objects.filter(year=year, quarter=quarter)\
                .annotate(score=F('response_rate') + F('attempts_rate') +
                          F('avrg_time_rate') + F('working_hours_rate') + F('feedback_rate') +
                          F('telegram_bot_rate') + F('behavior_rate'))
        return queryset


class CallCentreWorkListView(ListAPIView):
    pagination_class = CustomPagination
    serializer_class = serializers.CallCentreWorkSerializer
    queryset = CallCentre.objects.all()

    def get_queryset(self):
        request = self.request
        q = request.GET.get('search', '')
        lang = get_language()
        param = f'organization__name_{lang}'

        if q:
            entry_query = get_query(q, [param])
            queryset = CallCentre.objects.filter(entry_query)
        else:
            queryset = CallCentre.objects.all()
        return queryset


class CallCentreCallsInfoListView(ListAPIView):
    pagination_class = CustomPagination
    serializer_class = serializers.CallCentreCallsInfoListSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['score']

    def get_queryset(self):
        request = self.request
        q = request.GET.get('search', '')
        lang = get_language()
        param = f'call_centre__organization__name_{lang}'

        current_year = 2022
        current_quarter = 3
        year = int(self.request.GET.get('year', current_year))
        quarter = int(self.request.GET.get('quarter', current_quarter))
        if q:
            entry_query = get_query(q, [param])
            queryset = CallStatistics.objects.filter(year=year, quarter=quarter).annotate(
                score=Case(When(incomings__gt=0, then=(F('recieved') / F('incomings'))), default=0)).filter(entry_query)
        # queryset = CallStatistics.objects.filter(year=year, quarter=quarter).annotate(score=F('recieved') / F('incomings'))
        else:
            queryset = CallStatistics.objects.filter(year=year, quarter=quarter).annotate(
                score=Case(When(incomings__gt=0, then=(F('recieved') / F('incomings'))), default=0))
        return queryset


class CallCentreOperatorListView(ListAPIView):
    pagination_class = CustomPagination
    serializer_class = serializers.CallCentreOperatorSerializer
    queryset = CallCentre.objects.prefetch_related('periods').all()

    def get_queryset(self):
        request = self.request
        q = request.GET.get('search', '')
        lang = get_language()
        param = f'organization__name_{lang}'

        if q:
            entry_query = get_query(q, [param])
            queryset = CallCentre.objects.filter(entry_query)
        else:
            queryset = CallCentre.objects.all()
        return queryset

class CallCentreActivityListView(ListAPIView):
    pagination_class = CustomPagination
    serializer_class = serializers.CallCentreActivityListSerializer

    def get_queryset(self):
        request = self.request
        q = request.GET.get('search', '')
        lang = get_language()
        param = f'call_centre__organization__name_{lang}'

        current_year = 2022
        current_quarter = 3
        year = int(self.request.GET.get('year', current_year))
        quarter = int(self.request.GET.get('quarter', current_quarter))
        if q:
            entry_query = get_query(q, [param])
            queryset = ActivityStatistics.objects.filter(
                year=year, quarter=quarter).filter(entry_query)
        else:
            queryset = ActivityStatistics.objects.filter(
                year=year, quarter=quarter)
        return queryset


class CallCentreDetailsView(RetrieveAPIView):
    serializer_class = serializers.CallCentreDetailsSerializer
    queryset = CallCentre.objects.all()

    def get_queryset(self):
        request = self.request
        q = request.GET.get('search', '')
        lang = get_language()
        param = f'organization__name_{lang}'

        if q:
            entry_query = get_query(q, [param])
            queryset = CallCentre.objects.filter(entry_query)
        else:
            queryset = CallCentre.objects.all()
        return queryset