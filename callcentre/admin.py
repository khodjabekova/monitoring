from django.contrib import admin

from callcentre.models import CallCentre, Period, Rating, ActivityStatistics, CallStatistics
from django import forms


class PeriodInline(admin.TabularInline):
    model = Period
    extra = 1


class CallCentreForm(forms.ModelForm):
    class Meta:
        model = CallCentre
        exclude = (
            'created_by',
            'updated_by',
        )
        widgets = {
            'organization': forms.Select(attrs={'class': 'bootstrap-select', 'data-width': "80%"}),
        }


@admin.register(CallCentre)
class CallCentreAdmin(admin.ModelAdmin):
    list_display = ('organization', 'phone',)
    search_fields = ['phone', 'organization__name']
    list_filter = ('organization', )
    inlines = [PeriodInline, ]
    form = CallCentreForm


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        exclude = ('created_by', 'updated_by',)

        widgets = {
            'call_centre': forms.Select(attrs={'class': 'bootstrap-select', 'data-width': "80%"}),
            'response_note': forms.TextInput(attrs={'class': 'form-control', 'size': "80"}),
            'attempts_note': forms.TextInput(attrs={'class': 'form-control', 'size': "80"}),
            'avrg_time_note': forms.TextInput(attrs={'class': 'form-control', 'size': "80"}),
            'working_hours_note': forms.TextInput(attrs={'class': 'form-control', 'size': "80"}),
            'feedback_note': forms.Select(attrs={'class': 'bootstrap-select', 'style': 'width: 230% !important; resize: vertical !important;'}),
            'telegram_bot_note': forms.Select(attrs={'class': 'bootstrap-select', 'style': 'width: 230% !important; resize: vertical !important;'}),
            # 'behavior_note': forms.TextInput(attrs={'class': 'form-control', 'size': "80"}),
        }


@admin.register(Rating)
class RatingAdmin(admin.ModelAdmin):
    list_display = ('call_centre', 'year', 'quarter', 'total')
    list_filter = ('call_centre', 'year', 'quarter')

    # def get_readonly_fields(self, request, obj=None):
    #     if obj:
    #         return ('total',)
    #     return super().get_readonly_fields(request, obj)
    
    fields = ('call_centre', 'year', 'quarter',
              ('response_note', 'response_rate'),
              ('attempts_note', 'attempts_rate'),
              'attempts_note_ru',
              'attempts_note_uzb',
              ('avrg_time_note', 'avrg_time_rate'),
              ('working_hours_note', 'working_hours_rate'),
              ('feedback_note', 'feedback_rate'),
              ('telegram_bot_note', 'telegram_bot_rate'),
              'behavior_rate',
            #   'total',
              )
    form = RatingForm


@admin.register(CallStatistics)
class CallStatisticsAdmin(admin.ModelAdmin):
    list_display = ('call_centre', 'year', 'quarter', )
    list_filter = ('call_centre', 'year', 'quarter')
    exclude = ('created_by', 'updated_by',)

    # def get_readonly_fields(self, request, obj=None):
    #     if obj:
    #         return ('percent', 'missed')
    #     return super().get_readonly_fields(request, obj)


@admin.register(ActivityStatistics)
class ActivityStatisticsAdmin(admin.ModelAdmin):
    list_display = ('call_centre', 'year', 'quarter', )
    list_filter = ('call_centre', 'year', 'quarter')
    exclude = ('created_by', 'updated_by', 'working_hours_uz')
