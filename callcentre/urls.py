from django.urls import path

from . import views

urlpatterns = [
    path('', views.CallCentreListView.as_view()),
    path('rating', views.CallCentreRatingListView.as_view()),
    path('work', views.CallCentreWorkListView.as_view()),
    path('calls-info', views.CallCentreCallsInfoListView.as_view()),
    path('operator', views.CallCentreOperatorListView.as_view()),
    path('activity', views.CallCentreActivityListView.as_view()),
    path('<pk>', views.CallCentreDetailsView.as_view()),

]
