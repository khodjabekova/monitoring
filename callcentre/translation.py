from modeltranslation.translator import register, TranslationOptions

from .models import Rating as CallCentreRating,  ActivityStatistics


@register(CallCentreRating)
class CallCentreRatingTranslationOptions(TranslationOptions):
    fields = ('attempts_note',)

@register(ActivityStatistics)
class ActivityStatisticsTranslationOptions(TranslationOptions):
    fields = ('working_hours',)

