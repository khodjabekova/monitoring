from modeltranslation.translator import register, TranslationOptions

from department.models import Department
from document_type.models import DocumentType, MHHType, OrganizationType, Provider, ApprovingAuthority
from menu.models import Menu



@register(Department)
class DepartmentTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(DocumentType)
class DocumentTypeTranslationOptions(TranslationOptions):
    fields = ('name',)

@register(MHHType)
class MHHTypeTranslationOptions(TranslationOptions):
    fields = ('name',)

@register(OrganizationType)
class OrganizationTypeTranslationOptions(TranslationOptions):
    fields = ('name',)

@register(Provider)
class ProviderTranslationOptions(TranslationOptions):
    fields = ('name',)

@register(ApprovingAuthority)
class ApprovingAuthorityTranslationOptions(TranslationOptions):
    fields = ('name',)