"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
from .views import index_page

schema_view = get_schema_view(
    openapi.Info(
        title="Monitoring API",
        default_version='v1',
        description="Monitoring",
        license=openapi.License(name="Technocorp"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
    authentication_classes=[],
    url='https://monitoring.technocorp.uz/',
    # url='http://127.0.0.1:8000/',
)
urlpatterns = [
    # path('i18n/', include('django.conf.urls.i18n')), #uncomment this if you need admin translation

    path('admin/', admin.site.urls),
    
    path('swagger/', schema_view.with_ui('swagger',
                                         cache_timeout=10), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc',
                                       cache_timeout=0), name='schema-redoc'),
    # path('api/account/', include('account.urls')),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += i18n_patterns(
    path('api/account/', include('account.urls')),
    path('api/stats/', include('url.urls')),
    path('api/department/', include('department.urls')),
    path('api/organization/', include('organization.urls')),
    path('api/is/', include('information_system.urls')),
    path('api/website/', include('web_site.urls')),
    path('api/mobileapp/', include('mobileapp.urls')),
    path('api/globalip/', include('global_ip.urls')),
    path('api/callcentre/', include('callcentre.urls')),
    path('api/rating/', include('rating.urls')),
    path('api/strategicsite/', include('strategic_site.urls')),

)

urlpatterns += [
    path("", index_page),
]