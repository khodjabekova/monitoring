
def StatusType(code):
    if not code:
        return 'unknown'
    status_code = int(code)
    if status_code>= 200 and status_code < 400:
        return 'active'
    elif status_code >= 400:
        return 'failed'
    else:
        return 'unknown'