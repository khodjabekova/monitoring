from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
# from django_celery_beat.models import (ClockedSchedule, CrontabSchedule, IntervalSchedule,
#                      PeriodicTask, PeriodicTasks, SolarSchedule)
User._meta.verbose_name = 'Foydalanuvchi'
User._meta.verbose_name_plural = 'Foydalanuvchilar'

Group._meta.verbose_name = 'Rol'
Group._meta.verbose_name_plural = 'Rollar'

# admin.site.unregister(ClockedSchedule)
# admin.site.unregister(CrontabSchedule)
# admin.site.unregister(IntervalSchedule)
# admin.site.unregister(PeriodicTask)
# # admin.site.unregister(PeriodicTasks)
# admin.site.unregister(SolarSchedule)