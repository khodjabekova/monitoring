from django.db import models
from baseapp.models import BaseModel
from mptt.models import MPTTModel, TreeForeignKey


class DocumentType(BaseModel, MPTTModel):
    parent = TreeForeignKey('self', on_delete=models.SET_NULL,
                            null=True, blank=True, verbose_name="Yuqori tur")
    name = models.CharField(max_length=500, verbose_name="Nomi")

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'document_type'
        verbose_name = "Hujjat turi"
        verbose_name_plural = "Hujjat turlari"


class MHHType(BaseModel):
    name = models.CharField(max_length=255, verbose_name="Nomi")

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'mhh_type'
        verbose_name = "MHH turi"
        verbose_name_plural = "MHH turlari"


class Provider(BaseModel):
    name = models.CharField(max_length=255, verbose_name="Nomi")

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'provider'
        verbose_name = "Internet provayder"
        verbose_name_plural = "internet provayderlar"


class OrganizationType(BaseModel):
    name = models.CharField(max_length=255, verbose_name="Nomi")

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'organization_type'
        verbose_name = "Tashkilot turi"
        verbose_name_plural = "Tashkilot turlari"


class ApprovingAuthority(BaseModel):
    name = models.CharField(max_length=255, verbose_name="Nomi")

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'approving_authority'
        verbose_name = "Tasdiqlagan organ"
        verbose_name_plural = "Tasdiqlagan organ"
