from django.apps import AppConfig


class DocumentTypeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'document_type'
    verbose_name = "Qoshimcha ro‘yhatlar"
    verbose_name_plural = "Qoshimcha ro‘yhatlar"
