from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from .models import DocumentType, MHHType, OrganizationType, Provider, ApprovingAuthority
from django import forms


class DocumentTypetForm(forms.ModelForm):
    class Meta:
        model = DocumentType
        exclude = (
            'name_uz',
            'created_by',
            'updated_by',
        )
        widgets = {
            'parent': forms.Select(attrs={'class': 'bootstrap-select', 'data-width': "80%"})
        }


class DocumentTypeAdmin(MPTTModelAdmin):
    list_display = ('name', )
    form = DocumentTypetForm



admin.site.register(DocumentType, DocumentTypeAdmin)


class MHHTypeAdmin(admin.ModelAdmin):
    list_display = ('name', )

    exclude = (
            'name_uz',
            'created_by',
            'updated_by',
        )

admin.site.register(MHHType, MHHTypeAdmin)


class ProviderAdmin(admin.ModelAdmin):
    list_display = ('name', )

    exclude = (
            'name_uz',
            'created_by',
            'updated_by',
        )


admin.site.register(Provider, ProviderAdmin)


class OrganizationTypeAdmin(admin.ModelAdmin):
    list_display = ('name', )

    exclude = (
            'name_uz',
            'created_by',
            'updated_by',
        )

admin.site.register(OrganizationType, OrganizationTypeAdmin)

class ApprovingAuthorityAdmin(admin.ModelAdmin):
    list_display = ('name', )

    exclude = (
            'name_uz',
            'created_by',
            'updated_by',
        )

admin.site.register(ApprovingAuthority, ApprovingAuthorityAdmin)