from django.db import models
from django.utils.translation import gettext_lazy as _
from baseapp.models import BaseModel
from organization.models import Organization



class MobileApp(BaseModel):
    WORKING = 1
    NOT_WORKING = 2

    WORKING_CHOICE = (
        (WORKING, _('Ish faoliyatida')),
        (NOT_WORKING,  _('Ish faoliyatida emas')),
    )
    payment_system = models.BooleanField(default=False, verbose_name=_('To‘lov tizimlari'))
    name = models.CharField(max_length=255, verbose_name=_('Mobil ilova nomi'))
    organization = models.ForeignKey(Organization, on_delete=models.SET_NULL, null=True, blank=True, verbose_name=_('Ishlab chiquvchi tashkilot nomi'))
    organization_name = models.CharField(max_length=500, null=True, blank=True, verbose_name=_('Ishlab chiquvchi tashkilot nomi'))
    asos = models.CharField(max_length=255, null=True, blank=True, verbose_name=_('Asos'))
    maqsad = models.CharField(max_length=255, null=True, blank=True, verbose_name=_('Maqsad'))
    url_android = models.CharField(max_length=255, null=True, blank=True, verbose_name=_('URL manzili (Android)'))
    url_ios = models.CharField(max_length=255, null=True, blank=True, verbose_name=_('URL manzili (iOS)'))

    service = models.CharField(max_length=255, null=True, blank=True, verbose_name=_('Koʻrsatiladigan xizmatlar'))
    integration = models.IntegerField(null=True, blank=True, verbose_name=_('Integratsiya qilingan tizimlar soni'))
    integrated_systems = models.CharField(max_length=500, null=True, blank=True, verbose_name=_('Integratsiya qilingan tizimlar'))
    downloaded_android = models.IntegerField(null=True, blank=True, verbose_name=_('Foydalanuvchilar tomonidan yuklashlar soni (Android)'))
    downloaded_ios = models.IntegerField(null=True, blank=True, verbose_name=_('Foydalanuvchilar tomonidan yuklashlar soni(iOS)'))
    
    updated_android = models.DateField(max_length=255, null=True, blank=True, verbose_name=_('Soʻnggi yangilanish sanasi (Android)'))
    updated_ios = models.DateField(max_length=255, null=True, blank=True, verbose_name=_('Soʻnggi yangilanish sanasi soni(iOS)'))
    
    raiting_android = models.FloatField(null=True, blank=True, verbose_name=_('Qulayligi boʻyicha foydalanuvchilar bahosi (Android)'))
    raiting_ios = models.FloatField(null=True, blank=True, verbose_name=_('Qulayligi boʻyicha foydalanuvchilar bahosi (iOS)'))
    
    working_android = models.PositiveSmallIntegerField(
        choices=WORKING_CHOICE, default=NOT_WORKING, null=True, blank=True, verbose_name=_('Ish holati (Android)'))
    working_ios = models.PositiveSmallIntegerField(
        choices=WORKING_CHOICE, default=NOT_WORKING, null=True, blank=True, verbose_name=_('Ish holati (iOS)'))
    
    izoh = models.CharField(max_length=255, null=True, blank=True, verbose_name=_('Izoh'))

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'mobile_app'
        verbose_name = "Mobil ilova"
        verbose_name_plural = "Mobil ilovalar"