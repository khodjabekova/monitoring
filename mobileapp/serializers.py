from .models import MobileApp
from rest_framework import serializers



class MobileAppListSerializer(serializers.ModelSerializer):
    organization = serializers.SerializerMethodField()
    url_android = serializers.SerializerMethodField()
    url_ios = serializers.SerializerMethodField()
    working_android = serializers.SerializerMethodField()
    working_ios = serializers.SerializerMethodField()

    class Meta:
        model = MobileApp
        fields = ('id', 'name', 'organization', 'url_android', 'url_ios', 'working_android', 'working_ios')

    def get_organization(self, obj):
        if obj.organization:
            return obj.organization.name
        else:
            return obj.organization_name
        
    def get_url_android(self, obj):
        if obj.url_android:
            return True
        else:
            return False
        
    def get_url_ios(self, obj):
        if obj.url_ios:
            return True
        else:
            return False
        
    def get_working_android(self, obj):
        if obj.working_android == 1:
            return True
        else:
            return False
        
    def get_working_ios(self, obj):
        if obj.working_ios == 1:
            return True
        else:
            return False
    
class MobileAppDetailsSerializer(serializers.ModelSerializer):
    organization = serializers.SerializerMethodField()
    working_android = serializers.CharField(source='get_working_android_display', read_only=True)
    working_ios = serializers.CharField(source='get_working_ios_display', read_only=True)
    class Meta:
        model = MobileApp
        fields = ('name', 
                  'organization', 
                  'asos', 
                  'maqsad', 
                  'url_android',
                  'url_ios',
                  'service',
                  'integration',
                  'integrated_systems',
                  'downloaded_android',
                  'downloaded_ios',
                  'updated_android',
                  'updated_ios',
                  'raiting_android',
                  'raiting_ios',
                  'working_android',
                  'working_ios',
                  'izoh')

    def get_organization(self, obj):
        if obj.organization:
            return obj.organization.name
        else:
            return obj.organization_name