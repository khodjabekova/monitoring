from modeltranslation.translator import register, TranslationOptions

from .models import MobileApp


@register(MobileApp)
class OrganizationTranslationOptions(TranslationOptions):
    fields = ('name', 'organization_name', 'asos', 'maqsad', 'service', 'izoh', 'integrated_systems')