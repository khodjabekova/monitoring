from django.urls import path

from . import views

urlpatterns = [
    path('', views.MobileAppListView.as_view()),
    path('<pk>', views.MobileAppDetailsView.as_view()),

]
