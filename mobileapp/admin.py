from django.contrib import admin

from mobileapp.models import MobileApp

# Register your models here.


class MobileAppAdmin(admin.ModelAdmin):
    list_display = ('name', 'organization', 'payment_system')
    search_fields = ['name', ]
    list_filter = ('organization', 'payment_system')
    exclude = ('name_uz', 'organization_name_uz', 'asos_uz', 'integrated_systems_uz',
               'maqsad_uz', 'service_uz', 'izoh_uz','created_by', 'updated_by',)

    def organization(self, obj):
        if obj.organization:
            return obj.organization
        else:
            return obj.organization_name
   
    organization.short_description = 'Tashkilot'  


admin.site.register(MobileApp, MobileAppAdmin)