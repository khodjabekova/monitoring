# Generated by Django 3.2.4 on 2023-03-03 11:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mobileapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='mobileapp',
            name='integrated_systems',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Integratsiya qilingan tizimlar'),
        ),
    ]
