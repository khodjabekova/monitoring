# Generated by Django 3.2.4 on 2023-03-03 11:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mobileapp', '0002_mobileapp_integrated_systems'),
    ]

    operations = [
        migrations.AddField(
            model_name='mobileapp',
            name='integrated_systems_ru',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Integratsiya qilingan tizimlar'),
        ),
        migrations.AddField(
            model_name='mobileapp',
            name='integrated_systems_uz',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Integratsiya qilingan tizimlar'),
        ),
        migrations.AddField(
            model_name='mobileapp',
            name='integrated_systems_uzb',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Integratsiya qilingan tizimlar'),
        ),
    ]
