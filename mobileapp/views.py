from rest_framework.generics import ListAPIView, RetrieveAPIView
from config.paginations import CustomPagination
from . import serializers
from .models import MobileApp
from django_filters.rest_framework import DjangoFilterBackend

class MobileAppListView(ListAPIView):
    pagination_class = CustomPagination
    serializer_class = serializers.MobileAppListSerializer
    queryset = MobileApp.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['payment_system',]


class MobileAppDetailsView(RetrieveAPIView):
    serializer_class = serializers.MobileAppDetailsSerializer
    queryset = MobileApp.objects.all()
