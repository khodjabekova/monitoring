from django.contrib import admin

from .models import GlobalIP, IPAddress, Speed
from django import forms


class SpeedInline(admin.TabularInline):
    model = Speed
    extra = 1


class IPAddressInline(admin.TabularInline):
    model = IPAddress
    extra = 1


class GlobalIPForm(forms.ModelForm):
    class Meta:
        model = GlobalIP
        exclude = (
            'created_by',
            'updated_by',
        )

        widgets = {
            'organization': forms.Select(attrs={'class': 'bootstrap-select', 'data-width': "80%"})
        }


class GlobalIPAdmin(admin.ModelAdmin):
    list_display = ('organization', 'provider')
    list_filter = ('organization', 'provider')
    inlines = [IPAddressInline, SpeedInline]
    form = GlobalIPForm


admin.site.register(GlobalIP, GlobalIPAdmin)
