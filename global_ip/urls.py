from django.urls import path

from . import views

urlpatterns = [
    path('', views.GlobalIPListView.as_view()),
    path('<pk>', views.GlobalIPDetailsView.as_view()),

]
