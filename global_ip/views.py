from rest_framework.generics import ListAPIView, RetrieveAPIView
from config.paginations import CustomPagination
from . import serializers
from .models import GlobalIP


class GlobalIPListView(ListAPIView):
    pagination_class = CustomPagination
    serializer_class = serializers.GlobalIPListSerializer
    queryset = GlobalIP.objects.all()


class GlobalIPDetailsView(RetrieveAPIView):
    serializer_class = serializers.GlobalIPDetailsSerializer
    queryset = GlobalIP.objects.all()
