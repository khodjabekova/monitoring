from django.db import models
from django.utils.translation import gettext_lazy as _
from baseapp.models import BaseModel
from organization.models import Organization
from document_type.models import Provider


class GlobalIP(BaseModel):

    organization = models.ForeignKey(Organization, on_delete=models.SET_NULL, related_name='global_ip_list', null=True, blank=True, verbose_name=_('Idora nomi'))
    provider = models.ForeignKey(Provider, on_delete=models.SET_NULL, null=True, blank=True, verbose_name=_('Xizmat koʻrsatuvchi Internet operator va provayder nomi'))
    from_date = models.DateField(null=True, blank=True, verbose_name=_('Shartnomaga muvofiq xizmat koʻrsatish davri (dan)'))
    to_date = models.DateField(null=True, blank=True, verbose_name=_('Shartnomaga muvofiq xizmat koʻrsatish davri (gacha)'))


    # def __str__(self):
    #     return self.organization.name

    class Meta:
        db_table = 'global_ip'
        verbose_name = "Tashqi IP"
        verbose_name_plural = "Tashqi IPlar"


class IPAddress(models.Model):
    global_ip = models.ForeignKey(GlobalIP, on_delete=models.CASCADE, related_name='ip_list')
    # ip = models.GenericIPAddressField(protocol='both', unpack_ipv4=True, verbose_name='IP manzil')
    ip = models.CharField(max_length=100, verbose_name='IP manzil')

    def __str__(self):
        return self.ip

    class Meta:
        db_table = 'ip_address'
        verbose_name = "IP manzil"
        verbose_name_plural = "IP manzillar"

class Speed(models.Model):
    global_ip = models.ForeignKey(GlobalIP, on_delete=models.CASCADE, related_name='speed_list')
    speed = models.IntegerField(null=True, blank=True, verbose_name=_('Shartnoma boʻyicha taqdim etilayotgan Internet tezligi (MB/s)'))

    def __str__(self):
        return str(self.speed)

    class Meta:
        db_table = 'speed'
        verbose_name = "Tezlik"
        verbose_name_plural = "Tezliklar"