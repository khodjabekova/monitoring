from rest_framework import serializers

from .models import GlobalIP


class GlobalIPListSerializer(serializers.ModelSerializer):
    organization = serializers.SlugRelatedField(
        slug_field='name', read_only=True)

    class Meta:
        model = GlobalIP
        fields = ('id', 'organization')


class GlobalIPDetailsSerializer(serializers.ModelSerializer):
    provider = serializers.SlugRelatedField(slug_field='name', read_only=True)
    ip_list = serializers.SlugRelatedField(slug_field='ip', read_only=True, many=True)
    speed_list = serializers.SlugRelatedField(slug_field='speed', read_only=True, many=True)

    class Meta:
        model = GlobalIP
        fields = (
                  'provider',
                  'speed_list',
                  'from_date',
                  'to_date',
                  'ip_list')

