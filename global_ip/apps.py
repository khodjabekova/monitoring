from django.apps import AppConfig


class GlobalIpConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'global_ip'
    verbose_name = "Tashqi IP"
    verbose_name_plural = "Tashqi IPlar"