from django.db import models
from baseapp.models import BaseModel
from department.models import Department
from document_type.models import ApprovingAuthority, MHHType, OrganizationType, Provider

from django.utils.translation import gettext_lazy as _


class Organization(BaseModel):


    name = models.CharField(max_length=500, verbose_name="Nomi (to‘liq)")
    short_name = models.CharField(
        max_length=255, verbose_name="Nomi (qisqartmasi)")
    # Ташкил этилиши асоси
    base_mhh_type = models.ForeignKey(
        MHHType, null=True, blank=True, on_delete=models.SET_NULL, verbose_name="MHH turi")
    base_org = models.ForeignKey(ApprovingAuthority, on_delete=models.SET_NULL, null=True,
                                 blank=True, related_name="base_org", verbose_name="Tastiqlangan organ")

    base_number = models.CharField(
        max_length=255, null=True, blank=True, verbose_name="Raqami")
    base_date = models.DateField(null=True, blank=True, verbose_name="Sana")
    base_name = models.CharField(
        max_length=500, null=True, blank=True, verbose_name="Nomi")

    org_type = models.ForeignKey(OrganizationType, null=True, blank=True, on_delete=models.SET_NULL, verbose_name="Tashkilot turi")
    department = models.ForeignKey(Department, on_delete=models.SET_NULL, null=True, blank=True, related_name="sub_org",
                                   verbose_name="O‘zbekiston Respublikasi Vazirlar Mahkamasining kotibiyati / departamenti")

    # Ташкилот раҳбари
    org_ceo = models.CharField(
        max_length=255, null=True, blank=True, verbose_name="Tashkilot rahbari")
    org_ceo_position = models.CharField(
        max_length=255, null=True, blank=True, verbose_name="Lavozimi")
    press_id = models.IntegerField(null=True, blank=True, verbose_name="Matbuot-kotibi")
    active = models.BooleanField(default=True, verbose_name="Aktiv")

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'organization'
        verbose_name = "Tashkilot"
        verbose_name_plural = "Tashkilotlar"


"""
Ташкилот жойлашган манзили ҳамда боғланиш маълумотлари
"""


class Address(BaseModel):
    organization = models.OneToOneField(
        Organization, on_delete=models.CASCADE, related_name='address')
    address = models.CharField(max_length=500, verbose_name="Yuridik manzil")
    actual_address = models.CharField(
        max_length=500, verbose_name="Tashkilot joylashgan manzil")
    phone = models.CharField(max_length=17, null=True,
                             blank=True, verbose_name="Telefon raqam")
    email = models.EmailField(max_length=100, null=True,
                             blank=True, verbose_name="E-mail")
    exat = models.CharField(max_length=100, null=True,
                            blank=True, verbose_name="E-XAT")
    ijro_gov_uz = models.BooleanField(
        default=False, null=True, blank=True, verbose_name="IJRO.GOv.UZ tizimiga ulangan")

    def __str__(self):
        return self.organization.name

    class Meta:
        db_table = 'organization_address'
        verbose_name = "Manzil"
        verbose_name_plural = "Manzillar"


"""
Тузилма ва ходимлар тўғрисида маълумот
"""


class Info(BaseModel):
    organization = models.OneToOneField(
        Organization, on_delete=models.CASCADE, related_name='info')
    ma_divisions = models.IntegerField(
        null=True, blank=True, verbose_name="Markaziy apparatdagi tizim bo‘linmalari soni")
    regional_divisions = models.IntegerField(
        null=True, blank=True, verbose_name="Hududlardagi bo‘linmalari soni")
    organizations = models.IntegerField(
        null=True, blank=True, verbose_name="Tizimdagi tashkilotlar soni")
    ma_divisions_empl = models.IntegerField(
        null=True, blank=True, verbose_name="Markaziy apparatdagi xodimlar soni (shtat jadvaliga asosan)")
    regional_divisions_empl = models.IntegerField(
        null=True, blank=True, verbose_name="Hududiy bo‘linmalardagi xodimlar soni")
    organizations_empl = models.IntegerField(
        null=True, blank=True, verbose_name="Tizim tashkilotlaridagi xodimlar soni")

    def __str__(self):
        return self.organization.name

    class Meta:
        db_table = 'organization_info'
        verbose_name = "Tuzilma va xodimlar to‘g‘risida maʼlumot"
        verbose_name_plural = "Tuzilma va xodimlar to‘g‘risida maʼlumot"


"""
Ташкилотда АКТни жорий этиш бўйича бошқаруви
"""
class ICT(BaseModel):
    AVAILABLE = 1
    NOT_AVAILABLE = 2

    RESP_CHOICE = (
        (AVAILABLE, _('Available')),
        (NOT_AVAILABLE,  _('Not available')),
    )

    organization = models.OneToOneField(
        Organization, on_delete=models.CASCADE, related_name='ict')
    chief = models.CharField(max_length=255, null=True, blank=True,
                             verbose_name="Tashkilotda AKTni joriy etish bo‘yicha boshqaruvi")
    position = models.CharField(
        max_length=255, null=True, blank=True, verbose_name="Lavozimi")
    phone = models.CharField(max_length=17, null=True,
                             blank=True, verbose_name="Telefon raqam")
    email = models.EmailField(null=True, blank=True, verbose_name="E-mail")

    ict_responsible = models.PositiveSmallIntegerField(
        choices=RESP_CHOICE, null=True, blank=True, verbose_name="AKTga mas‘ul tarkibiy tuzilma mavjudligi")
    ict_department = models.CharField(max_length=255, null=True, blank=True,
                                      verbose_name="Tarkibiy tuzilmasini nomi")
    ict_department_chief = models.CharField(
        max_length=255, null=True, blank=True, verbose_name="Tarkibiy tuzilmasini boshlig‘i F.I.O.")
    ict_department_chief_position = models.CharField(
        max_length=255, null=True, blank=True, verbose_name="Lavozimi")
    ict_department_chief_phone = models.CharField(max_length=17, null=True,
                             blank=True, verbose_name="Telefon raqam")
    ict_department_chief_email = models.EmailField(null=True, blank=True, verbose_name="E-mail")
    ict_department_empl = models.IntegerField(
        null=True, blank=True, verbose_name="Shtat jadvaliga muvofiq xodimlar soni")

    # Information security
    is_responsible = models.PositiveSmallIntegerField(
        choices=RESP_CHOICE, null=True, blank=True, verbose_name="Axborot xavfsizligi boʼyicha masʼul tarkibiy tuzilma mavjudligi")
    is_department_chief = models.CharField(
        max_length=255, null=True, blank=True, verbose_name="Axborot xavfsizligi bo‘yicha masʼulning F.I.O.")
    is_department_chief_position = models.CharField(
        max_length=255, null=True, blank=True, verbose_name="Axborot xavfsizligi bo‘yicha masʼul hodimning lavozimi")
    is_department_chief_phone = models.CharField(max_length=17, null=True,
                             blank=True, verbose_name="Telefon raqam")
    is_department_chief_email = models.EmailField(null=True, blank=True, verbose_name="E-mail")
    is_department_empl = models.IntegerField(
        null=True, blank=True, verbose_name="Shtat jadvaliga muvofiq xodimlar soni")
    def __str__(self):
        return self.organization.name

    class Meta:
        db_table = 'organization_ict'
        verbose_name = "Tashkilotda AKTni joriy etish bo‘yicha boshqaruvi"
        verbose_name_plural = "Tashkilotda AKTni joriy etish bo‘yicha boshqaruvi"


class OutsourceOrg(BaseModel):
    organization = models.OneToOneField(
        Organization, on_delete=models.CASCADE, related_name='outsource')
    name = models.CharField(
        max_length=255, verbose_name="Autsorsing tashkiloti nomi")
    ceo = models.CharField(max_length=255, null=True, blank=True,
                           verbose_name="Autsorsing tashkiloti rahbari")
    contract_signed_date = models.DateField(
        null=True, blank=True, verbose_name="Shartnoma sanasi")
    contract_number = models.CharField(
        max_length=255, null=True, blank=True, verbose_name="Shartnoma ro‘yhat raqami")
    contract_expired_date = models.DateField(
        null=True, blank=True, verbose_name="Shartnoma muddati")
    certificate_signed_date = models.DateField(
        null=True, blank=True, verbose_name="Guvohnoma sana")
    certificate_number = models.CharField(
        max_length=255, null=True, blank=True, verbose_name="Guvohnoma ro‘yhat raqami")

    class Meta:
        db_table = 'outsource_org'
        verbose_name = "Autsorsing"
        verbose_name_plural = "Autsorsing"


"""
АКТ инфратузилмаси
"""
class Infrastructure(BaseModel):
    ADSL = 1
    FTTX = 2
    FTTB = 2
    GPON = 2

    TECH_CHOICE = (
        (ADSL, 'ADSL'),
        (FTTX, 'FTTX'),
        (FTTB, 'FTTB'),
        (GPON, 'GPON'),
    )
    organization = models.OneToOneField(
        Organization, on_delete=models.CASCADE, related_name='infrastructure')
    computers = models.IntegerField(
        null=True, blank=True, verbose_name="Kompyuterlar soni")
    servers = models.IntegerField(
        null=True, blank=True, verbose_name="Serverlar soni")
    internet_provider = models.ForeignKey(
        Provider, null=True, blank=True, on_delete=models.SET_NULL, verbose_name="Xizmat ko‘rsatuvchi Internet operator/provayder nomi")
    internet_tech = models.PositiveSmallIntegerField(
        choices=TECH_CHOICE, null=True, blank=True, verbose_name="Texnologiya")
    internet_speed = models.IntegerField(
        null=True, blank=True, verbose_name="Shartnoma bo‘yicha taqdim etilayotgan Internet tezligi (Mb/s)")

    def __str__(self):
        return self.organization.name

    class Meta:
        db_table = 'organization_infostructure'
        verbose_name = "AKT infratuzilmasi"
        verbose_name_plural = "AKT infratuzilmasi"


"""
Matbuot  
"""
# class Press(BaseModel):

#     organization = models.OneToOneField(
#         Organization, on_delete=models.CASCADE, related_name='press')
#     name = models.CharField(max_length=255, verbose_name="Matbuot-kotibi")
#     position = models.CharField(
#         max_length=255, null=True, blank=True, verbose_name="Lavozimi")
#     phone = models.CharField(max_length=17, null=True,
#                              blank=True, verbose_name="Telefon raqam")
#     fax = models.CharField(max_length=17, null=True,
#                              blank=True, verbose_name="Fax")
#     email = models.EmailField(null=True, blank=True, verbose_name="E-mail")

#     def __str__(self):
#         return self.name

#     class Meta:
#         db_table = 'organization_press'
#         verbose_name = "Matbuot-kotibi"
#         verbose_name_plural = "Matbuot-kotiblari"


"""
Ijtimoiy tarmoqlar
"""
class SocialMedia(BaseModel):

    organization = models.OneToOneField(
        Organization, on_delete=models.CASCADE, related_name='social')
    facebook = models.URLField(max_length=255, null=True, blank=True, verbose_name="Facebook")
    instagram = models.URLField(max_length=255, null=True, blank=True, verbose_name="Instagram")
    telegram = models.URLField(max_length=255, null=True, blank=True, verbose_name="Telegram")
    youtube = models.URLField(max_length=255, null=True, blank=True, verbose_name="YouTube")
    twitter = models.URLField(max_length=255, null=True, blank=True, verbose_name="Twitter")
    linkedin = models.URLField(max_length=255, null=True, blank=True, verbose_name="Linkedin")
    

    def __str__(self):
        return self.organization.name

    class Meta:
        db_table = 'organization_social'
        verbose_name = "Ijtimoiy tarmoqlar"
        verbose_name_plural = "Ijtimoiy tarmoqlar"