# Generated by Django 4.0.4 on 2023-01-24 10:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('department', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=500, verbose_name='Nomi (to‘liq)')),
                ('short_name', models.CharField(max_length=255, verbose_name='Nomi (qisqartmasi)')),
                ('base_mhh_type', models.PositiveSmallIntegerField(blank=True, choices=[(1, 'Konstitutsiya'), (2, 'Kodeks'), (3, 'Qonun')], null=True, verbose_name='MHH turi')),
                ('base_org', models.CharField(blank=True, max_length=255, null=True, verbose_name='Tastiqlangan organ')),
                ('base_number', models.CharField(blank=True, max_length=255, null=True, verbose_name='Raqami')),
                ('base_date', models.DateField(blank=True, null=True, verbose_name='Sana')),
                ('base_name', models.CharField(blank=True, max_length=255, null=True, verbose_name='Nomi')),
                ('org_type', models.PositiveSmallIntegerField(blank=True, choices=[(1, 'Vazirlik'), (2, 'Agentlik'), (3, 'Inspektsiya')], null=True, verbose_name='Tashkilot turi')),
                ('org_ceo', models.CharField(blank=True, max_length=255, null=True, verbose_name='Tashkilot rahbari')),
                ('org_ceo_position', models.CharField(blank=True, max_length=255, null=True, verbose_name='Lavozimi')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='%(app_label)s_%(class)s_created_by', to=settings.AUTH_USER_MODEL)),
                ('department', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='department.department', verbose_name='O‘zbekiston Respublikasi Vazirlar Mahkamasining kotibiyati / departamenti')),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='%(app_label)s_%(class)s_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Tashkilot',
                'verbose_name_plural': 'Tashkilotlar',
                'db_table': 'organization',
            },
        ),
        migrations.CreateModel(
            name='OutsourceOrg',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255, verbose_name='Autsorsing tashkiloti nomi')),
                ('ceo', models.CharField(blank=True, max_length=255, null=True, verbose_name='Autsorsing tashkiloti rahbari')),
                ('contract_signed_date', models.DateField(blank=True, null=True, verbose_name='Shartnoma sanasi')),
                ('contract_number', models.CharField(blank=True, max_length=255, null=True, verbose_name='Shartnoma ro‘yhat raqami')),
                ('contract_expired_date', models.DateField(blank=True, null=True, verbose_name='Shartnoma muddati')),
                ('certificate_signed_date', models.DateField(blank=True, null=True, verbose_name='Guvohnoma sana')),
                ('certificate_number', models.CharField(blank=True, max_length=255, null=True, verbose_name='Guvohnoma ro‘yhat raqami')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='%(app_label)s_%(class)s_created_by', to=settings.AUTH_USER_MODEL)),
                ('organization', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='outsource', to='organization.organization')),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='%(app_label)s_%(class)s_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Autsorsing',
                'verbose_name_plural': 'Autsorsing',
                'db_table': 'outsource_org',
            },
        ),
        migrations.CreateModel(
            name='Infrastructure',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('computers', models.IntegerField(blank=True, null=True, verbose_name='Kompyuterlar soni')),
                ('servers', models.IntegerField(blank=True, null=True, verbose_name='Serverlar soni')),
                ('internet_provider', models.CharField(blank=True, max_length=255, null=True, verbose_name='Xizmat ko‘rsatuvchi Internet operator/provayder nomi')),
                ('internet_tech', models.PositiveSmallIntegerField(blank=True, choices=[(1, 'ADSL'), (2, 'FTTX'), (2, 'FTTB'), (2, 'GPON')], null=True, verbose_name='Texnologiya')),
                ('internet_speed', models.IntegerField(blank=True, null=True, verbose_name='Shartnoma bo‘yicha taqdim etilayotgan Internet tezligi (Mb/s)')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='%(app_label)s_%(class)s_created_by', to=settings.AUTH_USER_MODEL)),
                ('organization', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='infrastructure', to='organization.organization')),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='%(app_label)s_%(class)s_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'AKT infratuzilmasi',
                'verbose_name_plural': 'AKT infratuzilmasi',
                'db_table': 'organization_infostructure',
            },
        ),
        migrations.CreateModel(
            name='Info',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('ma_divisions', models.IntegerField(blank=True, null=True, verbose_name='Markaziy apparatdagi tizim bo‘linmalari soni')),
                ('regional_divisions', models.IntegerField(blank=True, null=True, verbose_name='Hududlardagi bo‘linmalari soni')),
                ('organizations', models.IntegerField(blank=True, null=True, verbose_name='Tizimdagi tashkilotlar soni')),
                ('ma_divisions_empl', models.IntegerField(blank=True, null=True, verbose_name='Markaziy apparatdagi xodimlar soni (shtat jadvaliga asosan)')),
                ('regional_divisions_empl', models.IntegerField(blank=True, null=True, verbose_name='Hududiy bo‘linmalardagi xodimlar soni')),
                ('organizations_empl', models.IntegerField(blank=True, null=True, verbose_name='Tizim tashkilotlaridagi xodimlar soni')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='%(app_label)s_%(class)s_created_by', to=settings.AUTH_USER_MODEL)),
                ('organization', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='info', to='organization.organization')),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='%(app_label)s_%(class)s_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Tuzilma va xodimlar to‘g‘risida maʼlumot',
                'verbose_name_plural': 'Tuzilma va xodimlar to‘g‘risida maʼlumot',
                'db_table': 'organization_info',
            },
        ),
        migrations.CreateModel(
            name='ICT',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('chief', models.CharField(blank=True, max_length=255, null=True, verbose_name='Tashkilotda AKTni joriy etish bo‘yicha boshqaruvi')),
                ('position', models.CharField(blank=True, max_length=255, null=True, verbose_name='Lavozimi')),
                ('phone', models.CharField(blank=True, max_length=14, null=True, verbose_name='Telefon raqam')),
                ('email', models.EmailField(blank=True, max_length=254, null=True, verbose_name='E-mail')),
                ('ict_responsible', models.PositiveSmallIntegerField(blank=True, choices=[(1, 'Mavjud'), (2, 'Mavjud emas')], null=True, verbose_name='AKTga mas‘ul tarkibiy tuzilma mavjudligi')),
                ('ict_department', models.CharField(blank=True, max_length=255, null=True, verbose_name='Tarkibiy tuzilmasini nomi')),
                ('ict_department_chief', models.CharField(blank=True, max_length=255, null=True, verbose_name='Tarkibiy tuzilmasini boshlig‘i F.I.O.')),
                ('ict_department_chief_position', models.CharField(blank=True, max_length=255, null=True, verbose_name='Lavozimi')),
                ('ict_department_chief_phone', models.CharField(blank=True, max_length=14, null=True, verbose_name='Telefon raqam')),
                ('ict_department_chief_email', models.EmailField(blank=True, max_length=254, null=True, verbose_name='E-mail')),
                ('ict_department_empl', models.IntegerField(blank=True, null=True, verbose_name='Shtat jadvaliga muvofiq xodimlar soni')),
                ('is_responsible', models.PositiveSmallIntegerField(blank=True, choices=[(1, 'Mavjud'), (2, 'Mavjud emas')], null=True, verbose_name='Axborot xavfsizligi boʼyicha masʼul tarkibiy tuzilma mavjudligi')),
                ('is_department_chief', models.CharField(blank=True, max_length=255, null=True, verbose_name='Axborot xavfsizligi bo‘yicha masʼulning F.I.O.')),
                ('is_department_chief_position', models.CharField(blank=True, max_length=255, null=True, verbose_name='Axborot xavfsizligi bo‘yicha masʼul hodimning lavozimi')),
                ('is_department_chief_phone', models.CharField(blank=True, max_length=14, null=True, verbose_name='Telefon raqam')),
                ('is_department_chief_email', models.EmailField(blank=True, max_length=254, null=True, verbose_name='E-mail')),
                ('is_department_empl', models.IntegerField(blank=True, null=True, verbose_name='Shtat jadvaliga muvofiq xodimlar soni')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='%(app_label)s_%(class)s_created_by', to=settings.AUTH_USER_MODEL)),
                ('organization', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='ict', to='organization.organization')),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='%(app_label)s_%(class)s_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Tashkilotda AKTni joriy etish bo‘yicha boshqaruvi',
                'verbose_name_plural': 'Tashkilotda AKTni joriy etish bo‘yicha boshqaruvi',
                'db_table': 'organization_ict',
            },
        ),
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('address', models.CharField(max_length=500, verbose_name='Yuridik manzil')),
                ('actual_address', models.CharField(max_length=500, verbose_name='Tashkilot joylashgan manzil')),
                ('phone', models.CharField(blank=True, max_length=14, null=True, verbose_name='Telefon raqam')),
                ('email', models.EmailField(blank=True, max_length=100, null=True, verbose_name='E-mail')),
                ('exat', models.CharField(blank=True, max_length=100, null=True, verbose_name='E-XAT')),
                ('ijro_gov_uz', models.BooleanField(blank=True, default=False, null=True, verbose_name='IJRO.GOv.UZ tizimiga ulangan')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='%(app_label)s_%(class)s_created_by', to=settings.AUTH_USER_MODEL)),
                ('organization', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='address', to='organization.organization')),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='%(app_label)s_%(class)s_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Manzil',
                'verbose_name_plural': 'Manzillar',
                'db_table': 'organization_address',
            },
        ),
    ]
