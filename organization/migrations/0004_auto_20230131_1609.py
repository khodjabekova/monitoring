# Generated by Django 3.2.4 on 2023-01-31 11:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0003_auto_20230131_1203'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='phone',
            field=models.CharField(blank=True, max_length=17, null=True, verbose_name='Telefon raqam'),
        ),
        migrations.AlterField(
            model_name='ict',
            name='ict_department_chief_phone',
            field=models.CharField(blank=True, max_length=17, null=True, verbose_name='Telefon raqam'),
        ),
        migrations.AlterField(
            model_name='ict',
            name='is_department_chief_phone',
            field=models.CharField(blank=True, max_length=17, null=True, verbose_name='Telefon raqam'),
        ),
        migrations.AlterField(
            model_name='ict',
            name='phone',
            field=models.CharField(blank=True, max_length=17, null=True, verbose_name='Telefon raqam'),
        ),
    ]
