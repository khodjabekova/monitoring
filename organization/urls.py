from django.urls import path

from . import views

urlpatterns = [
    path('', views.OrganizationView.as_view()),
    path('<pk>', views.OrganizationRetrieveView.as_view()),
    path('<pk>/is', views.OrganizationISView.as_view()),
    path('<pk>/is/status', views.OrganizationISStatusView.as_view()),
    # path('<pk>/is/details', views.OrganizationISDetailsView.as_view()),
    path('<pk>/websites', views.OrganizationWSView.as_view()),
    path('<pk>/websites/status', views.OrganizationWebSiteStatusView.as_view()),
    # path('<pk>/websites/details', views.OrganizationWebSiteDetailsView.as_view()),
]
