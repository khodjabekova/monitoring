from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import AllowAny

from config.paginations import CustomPagination
from information_system.models import InformationSystem
from information_system.serializers import InformationSystemSerializer
from url.models import URLStat
from web_site.models import WebSite
from web_site.serializers import WebSiteSerializer
from . import serializers
from .models import Organization
from rest_framework import filters
from information_system.serializers import ISStatusSerializer
from web_site.serializers import WebSiteStatusSerializer

from django.db.models import OuterRef, Subquery



class OrganizationView(ListAPIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    pagination_class = CustomPagination
    serializer_class = serializers.OrganizationListSerializer
    queryset = Organization.objects.filter(active=True).all()

class OrganizationRetrieveView(RetrieveAPIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    serializer_class = serializers.OrganizationSerializer
    queryset = Organization.objects.filter(active=True).all()


class OrganizationISView(ListAPIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    pagination_class = CustomPagination
    serializer_class = serializers.InformationSystemSerializer

    def get_queryset(self):
        pk = self.kwargs['pk']
        return InformationSystem.objects.select_related('url').filter(organization_id=pk, active=True)

class OrganizationISStatusView(ListAPIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    pagination_class = CustomPagination
    serializer_class = ISStatusSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['status']

    def get_queryset(self):
        pk = self.kwargs['pk']
        queryset =  InformationSystem.objects.filter(active=True).select_related('url')\
            .annotate(status=Subquery(URLStat.objects.filter(url=OuterRef('url__id'),).order_by('-created_at').values('status_code')[:1]))\
            .filter(organization_id=pk)
        return queryset
        # return InformationSystem.objects.prefetch_related('url').annotate(status=F('url__last_status')).filter(organization_id=pk)

class OrganizationWSView(ListAPIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    pagination_class = CustomPagination
    serializer_class = WebSiteSerializer

    def get_queryset(self):
        pk = self.kwargs['pk']
        return WebSite.objects.filter(active=True, is_strategic=False).select_related('url').filter(organization_id=pk)


class OrganizationWebSiteStatusView(ListAPIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    pagination_class = CustomPagination
    serializer_class = WebSiteStatusSerializer
    # queryset = Organization.objects.prefetch_related('websites').all()
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['status']

    def get_queryset(self):
        pk = self.kwargs['pk']
        queryset =  WebSite.objects.filter(active=True, is_strategic=False).select_related('url')\
            .annotate(status=Subquery(URLStat.objects.filter(url=OuterRef('url__id'),).order_by('-created_at').values('status_code')[:1]))\
            .filter(organization_id=pk)
        return queryset
        # return WebSite.objects.prefetch_related('url').filter(organization_id=pk)
        # return WebSite.objects.prefetch_related('url').annotate(status=F('url__last_status')).filter(organization_id=pk)