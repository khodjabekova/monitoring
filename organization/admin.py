from django import forms
from django.contrib import admin
from organization.models import Organization, Address, Info, ICT, OutsourceOrg, Infrastructure, SocialMedia


class AddressInline(admin.StackedInline):
    model = Address
    extra = 1
    exclude = ('address_uz', 'actual_address_uz', 'created_by', 'updated_by')


class InfoInline(admin.StackedInline):
    model = Info
    extra = 1
    verbose_name = "Tuzilma"
    verbose_name_plural = "Tuzilma"
    exclude = ('created_by', 'updated_by')


class OutsourceOrgInline(admin.StackedInline):
    model = OutsourceOrg
    extra = 1
    exclude = ('name_uz', 'ceo_uz', 'created_by', 'updated_by')


class ICTInline(admin.StackedInline):
    model = ICT
    extra = 1
    verbose_name = "AKT"
    verbose_name_plural = "AKT"
    exclude = ('chief_uz', 'position_uz', 'ict_department_uz', 'ict_department_chief_uz',
               'ict_department_chief_position_uz', 'is_department_chief_uz', 'is_department_chief_position_uz', 'created_by', 'updated_by')


class InfrastructureInline(admin.StackedInline):
    model = Infrastructure
    extra = 1
    exclude = ('created_by', 'updated_by')


class SocialMediaInline(admin.StackedInline):
    model = SocialMedia
    extra = 1
    exclude = ('created_by', 'updated_by')
class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('name', 'org_type', 'department', 'active')
    search_fields = ['name', 'org_type__name', 'department__name']
    list_filter =  ('org_type', 'department')
    verbose_name = "Tuzilma to‘g‘risida maʼlumot"
    verbose_name_plural = "Tuzilma to‘g‘risida maʼlumotlar"
    # form = OrganizationForm
    inlines = [AddressInline, InfoInline, ICTInline,
               OutsourceOrgInline, InfrastructureInline, SocialMediaInline]
    exclude = ('name_uz', 'short_name_uz', 'base_name_uz', 'org_ceo_uz',
               'org_ceo_position_uz', 'created_by', 'updated_by',)

    # def formfield_for_foreignkey(self, db_field, request, **kwargs):
    #     print("!")
    #     if db_field.name == 'base_org':
    #         if 'object_id' in request.resolver_match.kwargs:
    #             parent_id = request.resolver_match.kwargs['object_id']
    #             kwargs['queryset'] = Department.objects.all()
    #     return super().formfield_for_foreignkey(db_field, request, **kwargs)

    # class Media:
    #     js = (
    #         '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
    #         '//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js',
    #         'phone.js',
    #     )


admin.site.register(Organization, OrganizationAdmin)
