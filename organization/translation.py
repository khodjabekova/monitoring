from modeltranslation.translator import register, TranslationOptions

from .models import Organization, Address, ICT, OutsourceOrg


@register(Organization)
class OrganizationTranslationOptions(TranslationOptions):
    fields = ('name', 'short_name', 'base_name', 'org_ceo', 'org_ceo_position')


@register(Address)
class AddressTranslationOptions(TranslationOptions):
    fields = ('address', 'actual_address')


@register(ICT)
class ICTTranslationOptions(TranslationOptions):
    fields = ('chief', 'position', 'ict_department', 'ict_department_chief',
              'ict_department_chief_position', 'is_department_chief', 'is_department_chief_position')


@register(OutsourceOrg)
class OutsourceOrgTranslationOptions(TranslationOptions):
    fields = ('name', 'ceo')


