
from rest_framework import serializers
import requests
from bs4 import BeautifulSoup
from global_ip.serializers import GlobalIPDetailsSerializer

from information_system.serializers import ISStatusSerializer, InformationSystemSerializer
from web_site.serializers import WebSiteSerializer, WebSiteStatusSerializer
from .models import Organization, Address, Info, ICT, OutsourceOrg, Infrastructure, SocialMedia
from django.utils.translation import gettext_lazy as _
from django.utils.translation import get_language


class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        fields = ('address', 'actual_address', 'phone',
                  'email', 'exat', 'ijro_gov_uz')


class InfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Info
        fields = ('ma_divisions', 'regional_divisions', 'organizations',
                  'ma_divisions_empl', 'regional_divisions_empl', 'organizations_empl')


class ICTSerializer(serializers.ModelSerializer):
    ict_responsible = serializers.CharField(
        source='get_ict_responsible_display', read_only=True)
    is_responsible = serializers.CharField(
        source='get_is_responsible_display', read_only=True)

    class Meta:
        model = ICT
        fields = ('chief', 'position', 'phone', 'email',
                  'ict_responsible',
                  'ict_department',
                  'ict_department_chief',
                  'ict_department_chief_position',
                  'ict_department_chief_phone',
                  'ict_department_chief_email',
                  'ict_department_empl',
                  'is_responsible',
                  'is_department_chief',
                  'is_department_chief_position',
                  'is_department_chief_phone',
                  'is_department_chief_email',
                  'is_department_empl',)


class OutsourceOrgSerializer(serializers.ModelSerializer):

    class Meta:
        model = OutsourceOrg
        fields = ('name', 'ceo',
                  'contract_signed_date',
                  'contract_number',
                  'contract_expired_date',
                  'certificate_signed_date',
                  'certificate_number')


class InfrastructureSerializer(serializers.ModelSerializer):
    internet_provider = serializers.SlugRelatedField(slug_field='name', read_only=True)
    class Meta:
        model = Infrastructure
        fields = ('computers',
                  'servers',
                  'internet_provider',
                  'internet_tech',
                  'internet_speed')


class PressSerializer(serializers.Serializer):
    name = serializers.CharField()
    position = serializers.CharField()
    phone = serializers.CharField()
    fax = serializers.CharField()
    email = serializers.CharField()

    class Meta:
        fields = ('name',
                  'position',
                  'phone',
                  'fax',
                  'email')


class SocialMediaSerializer(serializers.ModelSerializer):

    class Meta:
        model = SocialMedia
        fields = ('facebook',
                  'instagram',
                  'telegram',
                  'youtube',
                  'twitter',
                  'linkedin')


class OrganizationSerializer(serializers.ModelSerializer):
    address = AddressSerializer()
    base_mhh_type = serializers.SlugRelatedField(
        slug_field='name', read_only=True)
    base_org = serializers.SlugRelatedField(slug_field='name', read_only=True)
    department = serializers.SlugRelatedField(
        slug_field='name', read_only=True)
    info = InfoSerializer()
    ict = ICTSerializer()
    outsource = OutsourceOrgSerializer()
    infrastructure = InfrastructureSerializer()
    information_systems = InformationSystemSerializer(many=True)
    websites = WebSiteSerializer(many=True)
    org_type = serializers.SlugRelatedField(slug_field='name', read_only=True)
    press = serializers.SerializerMethodField()
    social = SocialMediaSerializer(read_only=True)
    global_ip_list = GlobalIPDetailsSerializer(many=True)

    class Meta:
        model = Organization
        fields = ('id', 'name', 'short_name', 'base_mhh_type', 'base_org', 'base_number', 'base_date', 'base_name', 'org_type', 'department', 'address',
                  'org_ceo', 'press', 'social', 'org_ceo_position',  'info', 'ict', 'outsource', 'infrastructure', 'global_ip_list', 'information_systems', 'websites')

    def get_press(self, obj):
        if not obj.press_id:
            return None
        try:
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
            }

            lang = get_language()

            if lang == 'uz':
                lang = 'oz'
            elif lang == 'uzb':
                lang = 'uz'

            url = f'https://gov.uz/{lang}/press/secretary/view/{obj.press_id}'

            response = requests.get(url=url, headers=headers)
            soup = BeautifulSoup(response.text)

            htmltable = soup.find_all(
                'table', {'class': 'table table-bordered'})
            text = htmltable[1].text.replace("\n", "  ").split(" ")
            new_list = list(filter(None, text))
            result = {'name': '',  'position': '',
                      'phone': '', 'fax': '', 'email': ''}
            key = 'name'
            i = 0

            for e in new_list:
                if not ':' in e:
                    result[key] = (result[key] + ' ' + e).lstrip()
                else:
                    i += 1
                    key = list(result.keys())[i]

            return PressSerializer(result).data
        except:
            return None


class OrganizationListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Organization
        fields = ('id', 'name')


class OrganizationISStatusSerializer(serializers.ModelSerializer):
    information_systems = ISStatusSerializer(many=True)

    class Meta:
        model = Organization
        fields = ('id', 'name', 'information_systems')


class OrganizationWebSitesSerializer(serializers.ModelSerializer):
    websites = WebSiteStatusSerializer(many=True)

    class Meta:
        model = Organization
        fields = ('id', 'name', 'websites')
