from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

class StrategicSiteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'strategic_site'
    verbose_name = _("Strategik saytlar reestri")
    verbose_name_plural = _("Strategik saytlar reestri")