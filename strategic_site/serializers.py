from .models import StrategicSite
from rest_framework import serializers



class StrategicSiteListSerializer(serializers.ModelSerializer):

    class Meta:
        model = StrategicSite
        fields = ('url', 'name',)

  