from django.contrib import admin
from .models import StrategicSite


@admin.register(StrategicSite)
class StrategicSiteAdmin(admin.ModelAdmin):
    list_display = ('name_uz', 'url', 'index')
    search_fields = ('name', 'url',)
    exclude = ('name_uz', 'created_by', 'updated_by',)
