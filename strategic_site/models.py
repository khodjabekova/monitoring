from django.db import models
from django.utils.translation import gettext_lazy as _

from baseapp.models import BaseModel



class StrategicSite(BaseModel):
    url = models.URLField(verbose_name='Havola')
    name = models.CharField(max_length=500, verbose_name='Nomi')
    index = models.IntegerField(null=True, blank=True, verbose_name='Tartib raqami')
    def __str__(self):
        return self.name

    class Meta:
        ordering = ['index']
        db_table = 'strategic_site'
        verbose_name = _("Strategik saytlar reestri")
        verbose_name_plural = _("Strategik saytlar reestri")
