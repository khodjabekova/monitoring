from modeltranslation.translator import register, TranslationOptions

from .models import StrategicSite


@register(StrategicSite)
class StrategicSiteTranslationOptions(TranslationOptions):
    fields = ('name', )

