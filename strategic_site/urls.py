from django.urls import path

from . import views

urlpatterns = [
    path('', views.StrategicSiteListView.as_view()),
]
