from rest_framework.generics import ListAPIView
from config.paginations import CustomPagination
from . import serializers
from .models import StrategicSite


class StrategicSiteListView(ListAPIView):
    pagination_class = CustomPagination
    serializer_class = serializers.StrategicSiteListSerializer
    queryset = StrategicSite.objects.all()
