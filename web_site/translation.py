from modeltranslation.translator import register, TranslationOptions

from .models import WebSite

@register(WebSite)
class WebSiteTranslationOptions(TranslationOptions):
    fields = ('name', 'content_manager', 'cm_position', )

