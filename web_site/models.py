from django.db import models
from baseapp.models import BaseModel
from config.utils import StatusType
from organization.models import Organization
from django.utils.translation import gettext_lazy as _
from url.models import URL

class WebSite(BaseModel):
    AVAILABLE = 1
    NOT_AVAILABLE = 2
    INCORRECT = 3

    CHOICE1 = (
        (AVAILABLE, _('Mavjud')),
        (NOT_AVAILABLE, _('Mavjud emas')),
        (INCORRECT, _('Notoʻgʻri oʻrnatilgan')),
    )

    WORKING = 1
    NOT_WORKING = 2

    CHOICE2 = (
        (WORKING, _('Ishchi')),
        (NOT_WORKING, _('Ish holatida emas')),
    )
    url = models.OneToOneField(URL, on_delete=models.CASCADE, related_name='website', null=True, blank=True, verbose_name="Havola")
    active = models.BooleanField(default=True)
    organization = models.ForeignKey(
        Organization, on_delete=models.CASCADE, related_name='websites', verbose_name="Tashkilot")
    is_main = models.BooleanField(
        default=False, verbose_name="Tashkilotning asosiy veb-sayti")
    is_strategic = models.BooleanField(
        default=False, verbose_name="Strategik sayt")
    # link = models.CharField(
    #     max_length=255, verbose_name="Rasmiy veb-sayti manzili")
    name = models.CharField(max_length=255, verbose_name="Veb-sayt nomi")
    logo = models.FileField(upload_to="website/", null=True, blank=True, verbose_name="Logotip")
    content_manager = models.CharField(max_length=255, null=True, blank=True,
                                       verbose_name="Rasmiy veb-saytga maʼlumotlar joylashtirishga masʼul xodim")
    cm_position = models.CharField(
        max_length=255, null=True, blank=True, verbose_name="Lavozim")
    cm_phone = models.CharField(
        max_length=14, null=True, blank=True, verbose_name="Telefon raqami")
    cm_email = models.EmailField(null=True, blank=True, verbose_name="E-mail")
    hosting = models.CharField(
        max_length=255, null=True, blank=True, verbose_name="Xosting")
    ip_address = models.CharField(
        max_length=255, null=True, blank=True, verbose_name="IP-manzili")
    # ssl_certificate = models.BooleanField(null=True,
    #                                       default=False, verbose_name="SSL-sertifikati mavjudligi")

    ssl_certificate_exist = models.PositiveSmallIntegerField(
        choices=CHOICE1, null=True, blank=True, default=None, verbose_name="Kriptografik (SSL/TLS) protokollar toʻgʻri sozlanganligi")
    ssl_certificate_working = models.PositiveSmallIntegerField(
        choices=CHOICE2, null=True, blank=True, default=None, verbose_name="Rasmiy veb-sayt ishchi holatdaligi")
    ssl_certificate_expiry_date = models.DateField(
        null=True, blank=True, verbose_name="SSL-sertifikati muddati")

    def __str__(self):
        return self.organization.name

    @property
    def last_status(self):
        status_type = StatusType(
            self.url.stats.latest('created_at').status_code)
        return status_type

    @property
    def last_status_code(self):
        return self.url.stats.latest('created_at').status_code

    class Meta:
        db_table = 'organization_website'
        verbose_name = "Veb-sayt"
        verbose_name_plural = "Veb-saytlar"
