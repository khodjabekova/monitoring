from django.db.models import OuterRef, Subquery
from django.utils.translation import get_language
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework import filters
from config.paginations import CustomPagination
from config.search import get_query
from url.models import URLStat

from . import serializers
from .models import WebSite


class WebSiteView(ListAPIView):
    serializer_class = serializers.WebSiteSerializer
    queryset = WebSite.objects.filter(active=True, is_strategic=False).all()


class WebSiteStatusView(ListAPIView):
    serializer_class = serializers.WebSiteStatusSerializer
    pagination_class = CustomPagination
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['status', 'name']
    filterset_fields = ['is_strategic']

    def get_queryset(self):
        request = self.request
        q = request.GET.get('search', '')
        try:
            strategic = (request.GET.get('strategic', '')).capitalize()
            if strategic != 'True':
                strategic = False
        except:
            strategic = False
        lang = get_language()
        param1 = f'name_{lang}'
        param2 = f'organization__name_{lang}'
        if q:
            entry_query = get_query(q, [param1, param2])
            queryset = WebSite.objects.filter(active=True, is_strategic=strategic).select_related('url')\
                .annotate(status=Subquery(URLStat.objects.filter(url=OuterRef('url'),).order_by('-created_at').values('status_code')[:1]))\
                .filter(entry_query)
        else:
            queryset = WebSite.objects.filter(active=True, is_strategic=strategic).select_related('url')\
                .annotate(status=Subquery(URLStat.objects.filter(url=OuterRef('url'),).order_by('-created_at').values('status_code')[:1]))\

        return queryset


class WebSiteRetrieveView(RetrieveAPIView):
    serializer_class = serializers.WebSiteDetailsSerializer
    queryset = WebSite.objects.filter(active=True).all()
