# Generated by Django 3.2.4 on 2023-01-26 05:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web_site', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='website',
            options={'verbose_name': 'Veb-sayt', 'verbose_name_plural': 'Veb-saytlar'},
        ),
    ]
