from rest_framework import serializers
from organization.models import Organization
from url.models import URL
from url.serializers import URLStatusSerializer
from .models import WebSite


class WebSiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = WebSite
        fields = ('id', 'url', 'name', )


class WebSiteOrganizationSerizlizer(serializers.ModelSerializer):

    class Meta:
        model = Organization
        fields = ('id', 'name')


class WebSiteURLSerializer(serializers.ModelSerializer):
    class Meta:
        model = URL
        fields = ('id', 'url', 'last_status', 'last_status_code',)


# class WebSiteStatusSerializer(serializers.ModelSerializer):
#     organization = WebSiteOrganizationSerizlizer(read_only=True)
#     url = WebSiteURLSerializer(read_only=True)

#     class Meta:
#         model = WebSite
#         fields = ('id', 'organization', 'url', 'name', )


class WebSiteStatusSerializer(serializers.ModelSerializer):
    url = URLStatusSerializer(read_only=True)

    class Meta:
        model = WebSite
        fields = ('id', 'name', 'url', 'logo')


class WebSiteDetailsSerializer(serializers.ModelSerializer):
    ssl_certificate = serializers.CharField(
        source='get_ssl_certificate_exist_display', read_only=True)
    ssl_certificate_working = serializers.CharField(
        source='get_ssl_certificate_working_display', read_only=True)

    class Meta:
        model = WebSite
        fields = ('id', 'name',
                  'is_main',
                  'content_manager',
                  'cm_position',
                  'cm_phone',
                  'cm_email',
                  'hosting',
                  'ip_address',
                  'ssl_certificate',
                  'ssl_certificate_working',
                  'ssl_certificate_expiry_date',)
