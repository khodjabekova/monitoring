from django.urls import path

from . import views

urlpatterns = [
    path('', views.WebSiteView.as_view()),
    path('status', views.WebSiteStatusView.as_view()),
    path('<pk>', views.WebSiteRetrieveView.as_view()),
]
