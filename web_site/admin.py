from django.contrib import admin
from .models import WebSite
from url.models import URL, URLStat
from django.db.models import OuterRef, Subquery, Q
from django_reverse_admin import ReverseModelAdmin
class URLInline(admin.StackedInline):
    model = URL
    extra = 1
    exclude = ('information_system', 'total_down', 'last_status',  'created_by', 'updated_by')

class LastStatusFilter(admin.SimpleListFilter):
    title = 'Last Status'
    parameter_name = 'last_status'

    def lookups(self, request, model_admin):
        return (
            ('active', 'Active'),
            ('failed', 'Failed'),
            ('unknown', 'Unknown'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'active':
            return queryset.annotate(latest_status=Subquery(URLStat.objects.filter(url=OuterRef('url__id'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__startswith='2') | Q(latest_status__startswith='3'))
        elif value == 'failed':
            return queryset.annotate(latest_status=Subquery(URLStat.objects.filter(url=OuterRef('url__id'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__startswith='4') | Q(latest_status__startswith='5'))
        elif value == 'unknown':
            return queryset.annotate(latest_status=Subquery(URLStat.objects.filter(url=OuterRef('url__id'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__isnull=True))

        return queryset
class WebSiteAdmin(ReverseModelAdmin):
    list_display = ('organization', 'name', 'url','last_status', 'last_status_code', 'is_strategic', 'active', 'ssl_certificate_exist')
    search_fields = ['name', 'organization__name', 'url__url']
    list_filter = ['organization','url', 'is_strategic', 'organization__org_type', LastStatusFilter]

    inline_reverse = [
                      ('url', {'fields': ['url', 'www_id', 'total_down', 'period']}),
                      ]
    inline_type = 'stacked'
    inlines = []
    verbose_name = "Veb-sayt"
    verbose_name_plural = "Veb-saytlar"

    fieldsets = (
        (None, {'fields': ('active', 'organization', 'is_main', 'is_strategic', 'name', 'logo')}),
        ("Maʼlumotlar joylashtirishga masʼul xodim ", {'fields': ('content_manager', 'cm_position', 'cm_phone', 'cm_email',)}),
        ("Server haqida ma‘lumotlar", {'fields': ('hosting', 'ip_address', 'ssl_certificate_exist', 'ssl_certificate_working', 'ssl_certificate_expiry_date')}),

    )
    # @admin.display(description='Tashkilot')
    # def get_org(self, obj):
    #     return obj.organization.name
    # get_org.admin_order_field = 'organization'

    # @admin.display(description='URL')
    # def get_url(self, obj):
    #     return obj.url.url
    # get_url.admin_order_field = 'url'


    exclude = ('name_uz', 'content_manager_uz', 'cm_position_uz','created_by', 'updated_by')


admin.site.register(WebSite, WebSiteAdmin)
