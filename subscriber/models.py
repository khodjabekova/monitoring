from django.db import models

# Create your models here.


class Subscriber(models.Model):
    name = models.CharField(default="", max_length=200, null=True, blank=True)
    chat_id = models.CharField(
        max_length=40, default="", editable=True, blank=True, unique=True)

    def __str__(self) -> str:
        return self.name

    class Meta:
        db_table = 'subscriber'
        verbose_name = "Telegram foydalanuchi"
        verbose_name_plural = "Telegram foydalanuchilar"
