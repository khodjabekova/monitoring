from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import AllowAny

from config.paginations import CustomPagination
from department.search import get_query
from . import serializers
from .models import Department
from organization.models import Organization
from django.utils.translation import get_language


class DepartmentView(ListAPIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    serializer_class = serializers.DepartmentListSerializer
    # queryset = Department.objects.all()

    def get_queryset(self):
        queryset = Department.objects.filter(level=0)
        return queryset


class DepartmentOrganizationsView(ListAPIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    pagination_class = CustomPagination
    serializer_class = serializers.DepartmentOrganizationsSerializer
    # queryset = Department.objects.all()

    def get_queryset(self):
        pk = self.kwargs['pk']
        request = self.request
        q = request.GET.get('search', '')
        lang = get_language()
        param1 = f'name_{lang}'
        if q:
            entry_query = get_query(q, [param1])
            queryset = Organization.objects.filter(active=True).filter(entry_query)

        else:
            if pk == 'all':
                queryset = Organization.objects.filter(active=True).order_by(param1)
            else:
                queryset = Organization.objects.filter(active=True).filter(
                    department_id=pk).order_by(param1)

        return queryset
