from organization.models import Organization
from .models import Department
from organization.serializers import OrganizationListSerializer

from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField


class DepartmentListSerializer(serializers.ModelSerializer):
    # sub_departments = RecursiveField(many=True)
    # sub_org = OrganizationListSerializer(many=True)

    class Meta:
        model = Department
        fields = ('id', 'name',)


class DepartmentOrganizationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = ('id', 'name')