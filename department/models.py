from django.db import models
from baseapp.models import BaseModel
from mptt.models import MPTTModel, TreeForeignKey

#TODO: delete parent and mptt
class Department(BaseModel, MPTTModel):
    """
    Class Department used in Organizations as supirior organization
    """
    parent = TreeForeignKey('self', related_name='sub_departments', on_delete=models.SET_NULL,
                            null=True, blank=True, verbose_name="Yuqori departament")
    name = models.CharField(max_length=500, verbose_name="Nomi")
    index = models.IntegerField(
        null=True, blank=True, verbose_name="Tartib raqami")


    def __str__(self):
        return self.name

    class MPTTMeta:
        order_insertion_by = ['index']

    class Meta:
        ordering = ['index']
        db_table = 'cm_department'
        verbose_name = "Departament"
        verbose_name_plural = "Departamentlar"
