from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from department.models import Department
from django import forms


class DepartmentForm(forms.ModelForm):
    class Meta:
        model = Department
        exclude = (
            'name_uz',
            'created_by',
            'updated_by',
        )
        widgets = {
            'parent': forms.Select(attrs={'class': 'bootstrap-select', 'data-width': "80%"})
        }


class DepartmentAdmin(admin.ModelAdmin):
    ordering = ['index']
    list_display = ('name', 'index')
    form = DepartmentForm


admin.site.register(Department, DepartmentAdmin)
