from django.urls import path

from . import views

urlpatterns = [
    path('', views.DepartmentView.as_view()),
    path('<pk>', views.DepartmentOrganizationsView.as_view()),

]
