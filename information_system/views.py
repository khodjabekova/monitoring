

# Create your views here.
from django.db.models import OuterRef, Subquery
from django.utils.translation import get_language
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework import filters
from config.paginations import CustomPagination
from config.search import get_query

from url.models import URLStat

from . import serializers
from .models import InformationSystem


class InformationSystemView(ListAPIView):
    serializer_class = serializers.InformationSystemSerializer
    queryset = InformationSystem.objects.filter(active=True).all()


class InformationSystemStatusView(ListAPIView):
    serializer_class = serializers.InformationSystemStatusSerializer
    pagination_class = CustomPagination
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['status', 'name']

    def get_queryset(self):
        request = self.request
        q = request.GET.get('search', '')
        lang = get_language()
        param1 = f'name_{lang}'
        param2 = f'organization__name_{lang}'
        
        if q:
            entry_query = get_query(q, [param1, param2])
            queryset = InformationSystem.objects.filter(active=True).select_related('url')\
                .annotate(status=Subquery(URLStat.objects.filter(url=OuterRef('url'),).order_by('-created_at').values('status_code')[:1]))\
                .filter(entry_query)
        else:
            queryset = InformationSystem.objects.filter(active=True).select_related('url')\
                .annotate(status=Subquery(URLStat.objects.filter(url=OuterRef('url'),).order_by('-created_at').values('status_code')[:1]))\
                
        return queryset


class InformationSystemRetrieveView(RetrieveAPIView):
    serializer_class = serializers.InformationSystemDetailsSerializer
    queryset = InformationSystem.objects.filter(active=True).all()
