from modeltranslation.translator import register, TranslationOptions

from .models import InformationSystem, LegalBasis, TechnicalDocuments, ExpertiseInfo, Tender, DataBaseInfo


@register(InformationSystem)
class InformationSystemTranslationOptions(TranslationOptions):
    fields = ('name', 'reason', 'admin', 'admin_position', 'dev_period')


@register(LegalBasis)
class LegalBasisTranslationOptions(TranslationOptions):
    fields = ('basis_name', 'internal_name')


@register(TechnicalDocuments)
class TechnicalDocumentsTranslationOptions(TranslationOptions):
    fields = ('name', 'signatory', 'signatory_position')


@register(Tender)
class TenderTranslationOptions(TranslationOptions):
    fields = ('tendet_winner', 'organization', 'cost', 'ownership')
    
@register(DataBaseInfo)
class DataBaseInfoTranslationOptions(TranslationOptions):
    fields = ('type', 'organization', 'server_address', 'tech_support')