from django.apps import AppConfig


class InformationSystemConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'information_system'
    verbose_name = "Axborot tizimi"
    verbose_name_plural = "Axborot tizimlari"