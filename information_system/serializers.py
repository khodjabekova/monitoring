from rest_framework import serializers

from url.serializers import URLStatusSerializer
from .models import DataBaseInfo, InformationSystem, LegalBasis, TechnicalDocuments, ExpertiseInfo, Tender
from organization.models import Organization
from url.models import URL

class InformationSystemSerializer(serializers.ModelSerializer):

    class Meta:
        model = InformationSystem
        fields = ('id', 'url', 'name', )


class ISOrganizationSerizlizer(serializers.ModelSerializer):

    class Meta:
        model = Organization
        fields = ('id', 'name')


class ISURLSerializer(serializers.ModelSerializer):
    class Meta:
        model = URL
        fields = ('id', 'url', 'last_status', 'last_status_code',)

class InformationSystemStatusSerializer(serializers.ModelSerializer):
    organization = ISOrganizationSerizlizer(read_only=True)
    url = ISURLSerializer(read_only=True)
    class Meta:
        model = InformationSystem
        fields = ('id', 'organization', 'url', 'name',)


class LegalBasisSerializer(serializers.ModelSerializer):
    basis_mhh = serializers.SlugRelatedField(slug_field='name', read_only=True)
    basis_approved = serializers.SlugRelatedField(
        slug_field='name', read_only=True)
    internal_mhh = serializers.SlugRelatedField(
        slug_field='name', read_only=True)
    internal_approved = serializers.SlugRelatedField(
        slug_field='name', read_only=True)
    accepted_for_use = serializers.CharField(
        source='get_accepted_for_use_display', read_only=True)
    accepted_for_impl = serializers.CharField(
        source='get_accepted_for_impl_display', read_only=True)

    class Meta:
        model = LegalBasis
        fields = ('basis_mhh',
                  'basis_approved',
                  'basis_number',
                  'basis_date',
                  'basis_name',
                  'internal_mhh',
                  'internal_approved',
                  'internal_number',
                  'internal_date',
                  'internal_name',
                  'accepted_for_use',
                  'accepted_for_use_file',
                  'accepted_for_impl',
                  'accepted_for_impl_file',
                  )


class TechnicalDocumentsSerializer(serializers.ModelSerializer):
    doc_type = serializers.SlugRelatedField(slug_field='name', read_only=True)

    class Meta:
        model = TechnicalDocuments
        fields = ('doc_type',
                  'name',
                  'date',
                  'signatory',
                  'signatory_position',
                  'signatory_phone',
                  'signatory_email',
                  )


class ExpertiseInfoSerializer(serializers.ModelSerializer):
    digital_expert = serializers.CharField(
        source='get_digital_expert_display', read_only=True)
    csec_expert = serializers.CharField(
        source='get_csec_expert_display', read_only=True)
    tech_assignment = serializers.CharField(
        source='get_tech_assignment_display', read_only=True)
    cybersecurity_expert = serializers.CharField(
        source='get_cybersecurity_expert_display', read_only=True)
    act = serializers.CharField(source='get_act_display', read_only=True)
    impl_doc = serializers.CharField(
        source='get_impl_doc_display', read_only=True)

    class Meta:
        model = ExpertiseInfo
        fields = ('digital_expert',
                  'digital_expert_file',
                  'csec_expert',
                  'csec_expert_file',
                  'tech_assignment',
                  'tech_assignment_file',
                  'cybersecurity_expert',
                  'cybersecurity_expert_file',
                  'act',
                  'act_number',
                  'act_date',
                  'act_file',
                  'impl_doc',
                  'impl_doc_number',
                  'impl_doc_date',
                  'impl_doc_file',
                  )


class TenderSerializer(serializers.ModelSerializer):
    tender = serializers.CharField(source='get_tender_display', read_only=True)
    contract = serializers.CharField(
        source='get_contract_display', read_only=True)
    license = serializers.CharField(
        source='get_license_display', read_only=True)
    inner_doc_type = serializers.SlugRelatedField(
        slug_field='name', read_only=True)
    license_type = serializers.SlugRelatedField(
        slug_field='name', read_only=True)

    class Meta:
        model = Tender
        fields = ('tender',
                  'tender_date',
                  'tendet_winner',
                  'contract',
                  'organization',
                  'inner_doc_type',
                  'inner_doc_number',
                  'inner_doc_date',
                  'inner_doc_file',
                  'cost',
                  'start_date',
                  'end_date',
                  'license',
                  'license_type',
                  'license_number',
                  'license_date',
                  'license_file',
                  'ownership', )


class DataBaseInfoSerializer(serializers.ModelSerializer):
    hosting = serializers.CharField(
        source='get_hosting_display', read_only=True)

    class Meta:
        model = DataBaseInfo
        fields = ('hosting',
                  'type',
                  'organization',
                  'ip_address',
                  'server_address',
                  'tech_support',
                  )


class InformationSystemDetailsSerializer(serializers.ModelSerializer):
    legal_basis = LegalBasisSerializer()
    tech_docs = TechnicalDocumentsSerializer()
    expertise_info = ExpertiseInfoSerializer()
    tender = TenderSerializer()
    database_info = DataBaseInfoSerializer()
    location = serializers.CharField(
        source='get_location_display', read_only=True)

    class Meta:
        model = InformationSystem
        fields = ('name',
                  'reason',
                  'admin',
                  'admin_position',
                  'admin_phone',
                  'admin_email',
                  'server',
                  'ip_address',
                  'location',
                  'reestr_number',
                  'reestr_date',
                  'reestr_link',
                  'rating',
                  'dev_period',
                  'legal_basis',
                  'tech_docs',
                  'expertise_info',
                  'tender',
                  'database_info',
                  )


class ISStatusSerializer(serializers.ModelSerializer):
    url = URLStatusSerializer(read_only=True)

    class Meta:
        model = InformationSystem
        fields = ('id', 'url', 'name', )
