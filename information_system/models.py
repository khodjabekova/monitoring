from django.db import models
from baseapp.models import BaseModel
from django.core.validators import MinValueValidator, MaxValueValidator
from config.utils import StatusType
from document_type.models import ApprovingAuthority, DocumentType, MHHType
from organization.models import Organization
from django.utils.translation import gettext_lazy as _
from url.models import URL

        
PERCENTAGE_VALIDATOR = [MinValueValidator(0), MaxValueValidator(100)]

def directory(instance, filename):
    return 'information_system/{}/{}'.format(instance.id, filename)

def exp_dir(instance, filename):
    return 'information_system/{}/expert/{}'.format(instance.information_system.id, filename)

def legal_dir(instance, filename):
    return 'information_system/{}/legal/{}'.format(instance.information_system.id, filename)

def tender_dir(instance, filename):
    return 'information_system/{}/tender/{}'.format(instance.information_system.id, filename)
class InformationSystem(BaseModel):
    INSIDE = 1
    OUTSIDE = 2

    LOCAION_CHOICE = (
        (INSIDE, _('O‘zbekiston hududida')),
        (OUTSIDE, _('Boshqa mamlakatda')),
    )
    url = models.OneToOneField(URL, on_delete=models.CASCADE, related_name='information_system', null=True, blank=True, verbose_name="Havola")
    active = models.BooleanField(default=True)
    organization = models.ForeignKey(Organization, on_delete=models.PROTECT, related_name='information_systems', verbose_name="Tashkilot")
    name = models.TextField(verbose_name="Axborot tizimining nomi")
    # link = models.URLField(null=True, blank=True, verbose_name="Axborot tizimining manzili")
    reason = models.TextField(max_length=500, null=True, blank=True, verbose_name="Axborot tizimining maqsadi")
    admin = models.CharField(max_length=255, null=True, blank=True, verbose_name="Axborot tizimining maʼmuri F.I.O.")
    admin_position = models.CharField(max_length=255, null=True, blank=True, verbose_name="Lavozimi")
    admin_phone = models.CharField(max_length=14, null=True, blank=True, verbose_name="Telefon raqami")
    admin_email = models.EmailField(null=True, blank=True, verbose_name="Elektron pochtasi")
    server = models.CharField(max_length=255, null=True, blank=True, verbose_name="Axborot tizimi joylashgan server haqida maʼlumot")
    ip_address = models.CharField(max_length=50, null=True, blank=True, verbose_name="IP-manzili")
    location = models.PositiveSmallIntegerField(choices=LOCAION_CHOICE, null=True, blank=True, verbose_name="Axborot tizimining respublila hududida joylashganligi")
    reestr_number = models.CharField(max_length=50, null=True, blank=True, verbose_name="Ro‘yxatga olingan raqami")
    reestr_date = models.DateField(null=True, blank=True, verbose_name="Yaratilgan sana")
    reestr_link = models.URLField(max_length=255, null=True, blank=True, verbose_name="reestr.uz dagi havola")
    rating = models.DecimalField(max_digits=3, decimal_places=2, default=0.00, null=True, blank=True,  validators=PERCENTAGE_VALIDATOR, verbose_name="Reyting")
    dev_period = models.CharField(max_length=255, null=True, blank=True, verbose_name="Axborot tizimining ishlab chiqilishi muddati")
    
    def __str__(self):
        return self.name

    @property
    def last_status(self):
        if self.url and self.url.stats:
            status_type = StatusType(self.url.stats.latest('created_at').status_code)
            return status_type
        else:
            return ''
    @property
    def last_status_code(self):
        if self.url and self.url.stats:
            return self.url.stats.latest('created_at').status_code
        else:
            return ''
    class Meta:
        db_table = 'information_system'
        verbose_name = "Axborot tizimi"
        verbose_name_plural = "Axborot tizimlari"

# Ахборот тизимининг ҳуқуқий асослари
class LegalBasis(BaseModel):
    ACCEPTED = 1
    NOT_ACCEPTED = 2

    CHOICE = (
        (ACCEPTED, _('Qilingan')),
        (NOT_ACCEPTED, _('Qilinmagan')),
    )
    information_system = models.OneToOneField(InformationSystem, on_delete=models.CASCADE, related_name='legal_basis')
    # Аxborot tizimining ishlab chiqish uchun normativ asosi
    basis_mhh = models.ForeignKey(MHHType, null= True, blank= True, related_name="basis", on_delete=models.SET_NULL, verbose_name="Аxborot tizimining ishlab chiqish uchun normativ asosi (MHH turi)")
    basis_approved = models.ForeignKey(ApprovingAuthority, on_delete=models.SET_NULL, related_name = "normativ", null=True, blank=True, verbose_name="Tasdiqlagan organ")
    basis_number = models.CharField(max_length=50, null=True, blank=True, verbose_name="Raqami")
    basis_date = models.DateField(null=True, blank=True, verbose_name="Sana")
    basis_name = models.CharField(max_length=500, null=True, blank=True, verbose_name="Nomi")
    # Tashkilotning ichki hujjati
    internal_mhh = models.ForeignKey(MHHType, null= True, blank= True, related_name="internal", on_delete=models.SET_NULL, verbose_name="Tashkilotning ichki hujjati (MHH turi)")
    internal_approved = models.ForeignKey(ApprovingAuthority, on_delete=models.SET_NULL, related_name="internal", null=True, blank=True, verbose_name="Tasdiqlagan organ")
    internal_number = models.CharField(max_length=50, null=True, blank=True, verbose_name="Raqami")
    internal_date = models.DateField(null=True, blank=True, verbose_name="Sana")
    internal_name = models.CharField(max_length=500, null=True, blank=True, verbose_name="Nomi")
    # Axborot tizimini foydalanishga qabul qilinganligi
    accepted_for_use = models.PositiveSmallIntegerField(choices=CHOICE, null=True, blank=True, verbose_name="Axborot tizimini foydalanishga qabul qilinganligi")
    accepted_for_use_file = models.FileField(upload_to=legal_dir, null=True, blank=True, verbose_name="Ilova")
    # Amaliyotga joriy qilish toʼgʼrisida ichki hujjat qabul qilinganligi
    accepted_for_impl = models.PositiveSmallIntegerField(choices=CHOICE, null=True, blank=True, verbose_name="Amaliyotga joriy qilish to‘g‘risida ichki hujjat qabul qilinganligi")
    accepted_for_impl_file = models.FileField(upload_to=legal_dir, null=True, blank=True, verbose_name="Ilova")

    def __str__(self):
        return self.information_system.name

    class Meta:
        db_table = 'is_legal_basis'
        verbose_name = "Huquqiy asoslari"
        verbose_name_plural = "Huquqiy asoslari"

# Ахборот тизимининг лойиҳа-техник ҳужжатлари
class TechnicalDocuments(BaseModel):
    information_system = models.OneToOneField(InformationSystem, on_delete=models.CASCADE, related_name='tech_docs')
    doc_type = models.ForeignKey(DocumentType, null=True, blank=True, on_delete=models.SET_NULL, verbose_name="Hujjat turi")
    name = models.CharField(max_length=255, null=True, blank=True, verbose_name="Axborot tizimni hujjatdagi nomi")
    date = models.DateField(null=True, blank=True, verbose_name="Hujjat tasdiqlangan sanasi")
    signatory = models.CharField(max_length=255, null=True, blank=True, verbose_name="Imzolovchi F.I.O.")
    signatory_position = models.CharField(max_length=255, null=True, blank=True, verbose_name="Lavozim")
    signatory_phone = models.CharField(max_length=14, null=True, blank=True, verbose_name="Telefon raqami")
    signatory_email = models.EmailField(null=True, blank=True, verbose_name="Elektron pochtasi")
  
    def __str__(self):
        return self.information_system.name

    class Meta:
        db_table = 'is_tech_docs'
        verbose_name = "Loyiha-texnik hujjatlari"
        verbose_name_plural = "Loyiha-texnik hujjatlari"

# Экспертиза ҳамда фойдалнишга топшириш тўғрисида маълумот
class ExpertiseInfo(BaseModel):
    AVAILABLE = 1
    NOT_AVAILABLE = 2

    CHOICE1 = (
        (AVAILABLE, _('Mavjud')),
        (NOT_AVAILABLE, _('Mavjud emas')),
    )

    CARRIED_OUT = 1
    NOT_CARRIED_OUT = 2

    CHOICE2 = (
        (CARRIED_OUT, _('O‘tkazildi')),
        (NOT_CARRIED_OUT, _('O‘tkazilmadi')),
    )

    EXPERT_CHOICE = (
        _('Texnik topshiriq talablariga muvofiqligi boʻyicha ekspertiza xulosasi'),
        _('Raqamli texnologiyalar vazirligi ekspert xulosasi'),
        _('“Kiberxavfsizlik markazi” DUK ekspert xulosasi'),
        _('Axborot va kiberxavfsizlik talablariga muvofiqligi boʻyicha ekspertiza xulosasi'),
        )
    information_system = models.OneToOneField(InformationSystem, on_delete=models.CASCADE, related_name='expertise_info')
    digital_expert = models.PositiveSmallIntegerField(choices=CHOICE1, null=True, blank=True, verbose_name="Raqamli texnologiyalar vazirligidan ekspert xulosasi")
    digital_expert_file = models.FileField(upload_to=exp_dir, null=True, blank=True, verbose_name="Ilova")
    csec_expert = models.PositiveSmallIntegerField(choices=CHOICE1, null=True, blank=True, verbose_name="«Kiberxavfsizlik markazi» DUKdan olingan ekspert xulosasi")
    csec_expert_file = models.FileField(upload_to=exp_dir, null=True, blank=True, verbose_name="Ilova")
    tech_assignment = models.PositiveSmallIntegerField(choices=CHOICE2, null=True, blank=True, verbose_name="Axborot tizimi texnik topshiriq talablariga muvofiqligi yuzasidan «Kiberxavfsizlik markazi» DUK ekspertizadan o‘tzkazilganligi")
    tech_assignment_file = models.FileField(upload_to=exp_dir, null=True, blank=True, verbose_name="Ilova")
    cybersecurity_expert = models.PositiveSmallIntegerField(choices=CHOICE2, null=True, blank=True, verbose_name="Axborot tizimi axborot va kiberxavfsizlik talablariga muvofiqligi yuzasidan «Kiberxavfsizlik markazi» DUK ekspertizadan o‘tzkazilganligi")
    cybersecurity_expert_file = models.FileField(upload_to=exp_dir, null=True, blank=True, verbose_name="Ilova")
    act = models.PositiveSmallIntegerField(choices=CHOICE1, null=True, blank=True, verbose_name="Axborot tizimni joriy etish hamda foydalanishga topshirish va qabul-qilish jarayonidan o‘tkazilganligi to‘g‘risidagi hujjatlarni mavjudligi")
    act_number = models.CharField(max_length=50, null=True, blank=True, verbose_name="Raqami")
    act_date = models.DateField(null=True, blank=True, verbose_name="Sana")
    act_file = models.FileField(upload_to=exp_dir, null=True, blank=True, verbose_name="Ilova")

    impl_doc = models.PositiveSmallIntegerField(choices=CHOICE1, null=True, blank=True, verbose_name="Amaliyotga joriy qilish to‘g‘risida ichki hujjat qabul qilinganligi")
    impl_doc_number = models.CharField(max_length=50, null=True, blank=True, verbose_name="Raqami")
    impl_doc_date = models.DateField(null=True, blank=True, verbose_name="Sana")
    impl_doc_file = models.FileField(upload_to=exp_dir, null=True, blank=True, verbose_name="Ilova")

    def __str__(self):
        return self.information_system.name

    class Meta:
        db_table = 'is_expertise_info'
        verbose_name = "Ekspertiza"
        verbose_name_plural = "Ekspertiza"


# Тендер ёки танлов ҳужжатлари
class Tender(BaseModel):
    AVAILABLE = 1
    NOT_AVAILABLE = 2

    CHOICE1 = (
        (AVAILABLE, _('Mavjud')),
        (NOT_AVAILABLE, _('Mavjud emas')),
    )

    CARRIED_OUT = 1
    NOT_CARRIED_OUT = 2

    CHOICE2 = (
        (CARRIED_OUT, _('O‘tkazildi')),
        (NOT_CARRIED_OUT, _('O‘tkazilmadi')),
    )
    information_system = models.OneToOneField(InformationSystem, on_delete=models.CASCADE, related_name='tender')
    tender = models.PositiveSmallIntegerField(choices=CHOICE2, null=True, blank=True, verbose_name="Tender yoki tanlov oʼtkazilganligi")
    tender_date = models.DateField(null=True, blank=True, verbose_name="Tender yoki tanlov amalga oshirilgan muddati")
    tendet_winner = models.CharField(max_length=255, null=True, blank=True, verbose_name="Tender yoki tanlovda gʼolib boʼlgan tashkilot nomi")
    contract = models.PositiveSmallIntegerField(choices=CHOICE1, null=True, blank=True, verbose_name="Yollanma tashqi dasturchilar yoki dasturiy mahsulotlar ishlab chiquvchi korxonalarni jalb qilish orqali ishlab chiqilgan, joriy etilgan hamda takomillashtirilgan loyiha uchun tuzilgan shartnoma mavjudligi")
    organization = models.CharField(max_length=255, null=True, blank=True, verbose_name="Tashkilot nomi")
    inner_doc_type = models.ForeignKey(DocumentType, null=True, blank=True, on_delete=models.SET_NULL, related_name="inner_doc", verbose_name="Hujjat turi")
    inner_doc_number = models.CharField(max_length=255, null=True, blank=True, verbose_name="Hujjat raqami")
    inner_doc_date = models.DateField(null=True, blank=True, verbose_name="Hujjat sanasi")
    inner_doc_file = models.FileField(upload_to=tender_dir, null=True, blank=True, verbose_name="Ilova")
    cost = models.CharField(max_length=255, null=True, blank=True, verbose_name="Loyiha summasi")
    start_date = models.DateField(null=True, blank=True, verbose_name="Tizimni yaratish sanasi")
    end_date = models.DateField(null=True, blank=True, verbose_name="Tizimni yakunlash sanasi")
    license = models.PositiveSmallIntegerField(choices=CHOICE1, null=True, blank=True, verbose_name="Axborot tizimni litsenziyasi yoki patenti mavjudligi ")
    license_type = models.ForeignKey(DocumentType, null=True, blank=True, on_delete=models.SET_NULL, related_name="license", verbose_name="Hujjat turi")
    license_number = models.CharField(max_length=255, null=True, blank=True, verbose_name="Hujjat raqami")
    license_date = models.DateField(null=True, blank=True, verbose_name="Hujjat sanasi")
    license_file = models.FileField(upload_to=tender_dir, null=True, blank=True, verbose_name="Ilova")
    ownership = models.CharField(max_length=255, null=True, blank=True, verbose_name="Axborot tizimga egalik huquqi qilinishi")

    def __str__(self):
        return self.information_system.name

    class Meta:
        db_table = 'is_tender'
        verbose_name = "Tender yoki tanlov hujjatlari"
        verbose_name_plural = "Tender yoki tanlov hujjatlari"


class DataBaseInfo(BaseModel):
    AVAILABLE = 1
    NOT_AVAILABLE = 2

    CHOICE = (
        (AVAILABLE, _('Mavjud')),
        (NOT_AVAILABLE, _('Mavjud emas')),
    )
    information_system = models.OneToOneField(InformationSystem, on_delete=models.CASCADE, related_name='database_info')
    hosting = models.PositiveSmallIntegerField(choices=CHOICE, null=True, blank=True, verbose_name="Co-location yoki Xosting xizmati mavjudligi")
    type = models.CharField(max_length=255, null=True, blank=True, verbose_name="Xizmat turi")
    organization = models.CharField(max_length=255, null=True, blank=True, verbose_name="Tashkilot nomi")
    ip_address = models.CharField(max_length=255, null=True, blank=True, verbose_name="Xosting xizmatidan foydalanilganda Respublikada yoki Xorijdagi o‘zgarmas (stat) IP-manzili")
    server_address = models.CharField(max_length=500, null=True, blank=True, verbose_name="Manzil")

    tech_support = models.CharField(max_length=255, null=True, blank=True, verbose_name="Axborot tizimni texnik-qo‘llab quvvatlash bo‘yicha xizmat ko‘rsatuvchi tashkilot nomi")

    def __str__(self):
        return self.information_system.name


    class Meta:
        db_table = 'is_database_info'
        verbose_name = "Maʼlumotlar bazasi"
        verbose_name_plural = "Maʼlumotlar bazasi"
