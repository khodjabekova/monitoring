from django.contrib import admin
from .models import InformationSystem, LegalBasis, TechnicalDocuments, ExpertiseInfo, Tender, DataBaseInfo
from url.models import URL, URLStat
from django.db.models import OuterRef, Subquery, Q
from django_reverse_admin import ReverseModelAdmin

class LegalBasisInline(admin.StackedInline):
    model = LegalBasis
    extra = 1
    exclude = ('basis_name_uz', 'internal_name_uz', 'created_by', 'updated_by')


class TechnicalDocumentsInline(admin.StackedInline):
    model = TechnicalDocuments
    extra = 1
    exclude = ('name_uz', 'signatory_uz', 'signatory_position_uz',
               'created_by', 'updated_by')


class ExpertiseInfoInline(admin.StackedInline):
    model = ExpertiseInfo
    extra = 1
    exclude = ('created_by', 'updated_by')


class TenderInline(admin.StackedInline):
    model = Tender
    extra = 1
    exclude = ('tendet_winner_uz', 'organization_uz', 'cost_uz',
               'ownership_uz', 'created_by', 'updated_by')


class DataBaseInfoInline(admin.StackedInline):
    model = DataBaseInfo
    extra = 1
    exclude = ('type_uz', 'organization_uz', 'server_address_uz',
               'tech_support_uz', 'created_by', 'updated_by')


# class URLInline(admin.StackedInline):
#     model = URL
#     extra = 1
#     exclude = ('website', 'total_down', 'last_status',
#                'created_by', 'updated_by')


class LastStatusFilter(admin.SimpleListFilter):
    title = 'Last Status'
    parameter_name = 'last_status'

    def lookups(self, request, model_admin):
        return (
            ('active', 'Active'),
            ('failed', 'Failed'),
            ('unknown', 'Unknown'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'active':
            return queryset.annotate(latest_status=Subquery(URLStat.objects.filter(url=OuterRef('url__id'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__startswith='2') | Q(latest_status__startswith='3'))
        elif value == 'failed':
            return queryset.annotate(latest_status=Subquery(URLStat.objects.filter(url=OuterRef('url__id'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__startswith='4') | Q(latest_status__startswith='5'))
        elif value == 'unknown':
            return queryset.annotate(latest_status=Subquery(URLStat.objects.filter(url=OuterRef('url__id'),).order_by('-created_at').values('status_code')[:1])).filter(Q(latest_status__isnull=True))

        return queryset


class InformationSystemAdmin(ReverseModelAdmin):

    list_display = ('name', 'organization', 'url',
                    'last_status', 'last_status_code', 'active')
    search_fields = ['name', 'organization__name', 'url__url']
    list_filter = ['organization', 'url', 'active', LastStatusFilter]
    # list_filter = (LastStatusFilter,)
    verbose_name = "Axborot tizimi"
    verbose_name_plural = "Axborot tizimi"
    inline_reverse = [
                      ('url', {'fields': ['url', 'www_id', 'total_down', 'period']}),
                      ]
    inline_type = 'stacked'
    inlines = [LegalBasisInline, TechnicalDocumentsInline,
               ExpertiseInfoInline, TenderInline, DataBaseInfoInline,]
    
    exclude = ('name_uz', 'reason_uz', 'admin_uz', 'admin_position_uz',
               'dev_period_uz', 'created_by', 'updated_by')


admin.site.register(InformationSystem, InformationSystemAdmin)
