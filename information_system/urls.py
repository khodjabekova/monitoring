from django.urls import path

from . import views

urlpatterns = [
    path('', views.InformationSystemView.as_view()),
    path('status', views.InformationSystemStatusView.as_view()),
    path('<pk>', views.InformationSystemRetrieveView.as_view()),
]
